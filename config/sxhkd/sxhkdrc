
#
# bspwm hotkeys
#

# quit bspwm normally
super + alt + Escape
  pkill -x panel; bspc quit

# close and kill
super + {_,shift + }w
	bspc node -{c,k}

# alternate between the tiled and monocle layout
alt + @space
	bspc desktop -l next

# if the current node is automatic, send it to the last manual, otherwise pull
# the last leaf
super + y
	bspc query -N -n focused.automatic && bspc node -n last.!automatic || bspc node last.leaf -n focused

# swap the current node and the biggest node
super + g
	bspc node -s biggest

# balance desktop areas
super + b
	bspc node @/ -B

# rotate desktop structure
super + shift + {comma,period,slash}
  bspc node @/ -R {90,270,180}

# rotate all the windows within the desktops while maintaining desktop structure
super + {comma,period}
	bspc node @/ -C {backward,forward}

#
# bspwm - mouse controls
#

## click window to focus
#~button1
#	bspc pointer -g focus
#
## if you super and drag on a window, it will either resize or swap places with
## another window
#super + button{1-3}
#  bspc pointer -g {move,resize_side,resize_corner}
#
#super + !button{1-3}
#  bspc pointer -t %i %i
#
#super + @button{1-3}
#	bspc pointer -u

#
# state/flags
#

# set the window state
super + {t,shift + t,s,f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
super + ctrl + {x,y,z}
	bspc node -g {locked,sticky,private}

#
# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
super + {p,b,comma,period}
	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
super + {_,shift + }c
	bspc node -f {next,prev}.local

# alt+tab, rotate focus
alt + {_,shift + }Tab
	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
super + bracket{left,right}
super + {Left,Right}
	bspc desktop -f {prev,next}.local

# focus the last node/desktop
super + {grave,shift + grave}
	bspc {node,desktop} -f last

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc {desktop -f,node -d} '^{1-9,10}'

#
# preselect
#

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + ctrl + {Left,Down,Up,Right}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + ctrl + shift + {Left,Down,Up,Right}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
ctrl + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# swap current window with last selected window
#super + apostrophe
#	bspc node -s last

# move selected window to prev/next desktop and follow
ctrl + alt + {Left,Right}
	bspc node -d {prev,next} -f

########################
# wm-independent hotkeys
########################

#
# Application Launchers
#

alt + shift + a
  runOrRaise Ardour5
super + shift + a
  xdotool search --onlyvisible Ardour5 windowunmap || xdotool search Ardour5 windowmap

alt + shift + j
  runOrRaise qjackctl
super + shift + j
  xdotool search --onlyvisible qjackctl windowunmap || xdotool search qjackctl windowmap

alt + shift + s
  runOrRaise nsm.sh
super + shift + s
  xdotool search --onlyvisible nsm.sh windowunmap || xdotool search nsm.sh windowmap

alt + shift + h
  runOrRaise hydrogen
super + shift + h
  xdotool search --onlyvisible hydrogen windowunmap || xdotool search hydrogen windowmap

# floating application launcher
#super + slash
#  lighthouse | sh
super + slash
  dmenu_run

# urxvt term with screen
super + BackSpace
	urxvt -e screen

# urxvt term without screen
super + shift + BackSpace
	urxvt


# lauchers and togglers
#
# launchers - actually start the app
# togglers  - show/hide them on the current desktop if they're already running
# on that desktop

#alt + shift + b
#  runOrRaise uzbl-tabbed
#super + shift + b
#  xdotool search --onlyvisible uzbl-tabbed windowunmap || xdotool search uzbl-tabbed windowmap

alt + shift + b
  runOrRaise google-chrome
super + shift + b
  xdotool search --onlyvisible google-chrome windowunmap || xdotool search google-chrome windowmap

ctrl + super + b
  killall -9 spotify

alt + shift + c
  runOrRaise google-chrome
super + shift + c
  xdotool search --onlyvisible google-chrome windowunmap || xdotool search google-chrome windowmap

alt + shift + d
  runOrRaise nautilus
super + shift + d
  xdotool search --onlyvisible nautilus windowunmap || xdotool search nautilus windowmap

alt + shift + i
  runOrRaise inkscape
super + shift + i
  xdotool search --onlyvisible inkscape windowunmap || xdotool search inkscape windowmap

alt + shift + g
  runOrRaise gimp
super + shift + g
  xdotool search --onlyvisible gimp windowunmap || xdotool search gimp windowmap

alt + shift + l
  runOrRaise calibre
super + shift + l
  xdotool search --onlyvisible calibre windowunmap || xdotool search calibre windowmap

alt + shift + n
  runOrRaise nixnote2
super + shift + n
  xdotool search --onlyvisible nixnote2 windowunmap || xdotool search nixnote2 windowmap

alt + shift + m
  runOrRaise spotify
super + shift + m
  xdotool search --onlyvisible spotify windowunmap || xdotool search spotify windowmap

ctrl + super + m
  killall -9 spotify

alt + shift + z
  runOrRaise zotero
super + shift + z
  xdotool search --onlyvisible zotero windowunmap || xdotool search zotero windowmap

alt + shift + v
  runOrRaise virt-manager
super + shift + v
  xdotool search --onlyvisible virt-manager windowunmap || xdotool search virt-manager windowmap

alt + shift + e
  runOrRaise 'emacsclient -ca ""'
super + shift + e
  xdotool search --onlyvisible "emacsclient -ca\"\"" windowunmap || xdotool search "emacsclient -c\"\""  windowmap

alt + shift + p
  runOrRaise "pavucontrol"
super + shift + p
  xdotool search --onlyvisible "pavucontrol" windowunmap || xdotool search "pavucontrol"  windowmap

#
# Mutlimedia Config
#

XF86AudioRaiseVolume
  media-controller.sh -n dunstify volume-increase

XF86AudioLowerVolume
  media-controller.sh -n dunstify volume-decrease

XF86AudioMute
  media-controller.sh -n dunstify toggle-mute

XF86HomePage
  google-chrome ${HOME}/.config/infoconf/html

XF86Search
  google-chrome

# Switch to prev song
super + alt + Left
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 prev

# Switch to next song
super + alt + Right
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 next

# Toggle pause/play
super + alt + p
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 toggle

# TODO: Fix up append functionality within mpc.sh. They aren't working as
# expected.

# Append 10 random songs to playlist
super + alt + 1
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 random-append

# Append random album to playlist
super + alt + 2
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 album-append

# Append current song's album to playlist
super + alt + 3
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 current-append

# Decrease vol
super + alt + Down
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 volume-decrease

# Increase vol
super + alt + Up
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 volume-increase

# Update music library database
super + alt + u
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 update

# Toggle random mode
super + alt + z
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 shuffle

# Toggle repeat mode
super + alt + r
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 repeat

# Toggle single mode
super + alt + y
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 single

# Display current song info in a notification pop-up
super + alt + c
  mpc.sh -n dunstify -s 127.0.0.1 -p 6600 current

#
# Sound cards
#

# TODO: This needs clean-up work in media-controller.sh

# Switch between available audio devices within jackd
super + alt + a
  media-controller.sh -n dunstify toggle

# Switch between available hardware devices outside of jackd
super + alt + t
  media-controller.sh -n dunstify toggle-sound-card

# Display current sound card info in a notification pop-up
super + alt + s
  media-controller.sh -n dunstify current-sound-card

#
# text-to-speech (tts)
#

# Read the selected text out loud through the sound card
alt + Escape
  tts speak

# Increase tts speech rate
ctrl + alt + k
  tts -n dunstify rate-increase

# Decrease tts speech rate
ctrl + alt + j
  tts -n dunstify rate-decrease

#
# screenshots
#

Print
  screenshot 0

super + Print
  screenshot 0 "" -u

#
# scratchpads
#

super + Tab
    xdotool search --onlyvisible --classname scratchpad windowunmap || xdotool search --classname scratchpad windowmap || urxvtc_mod -name scratchpad -e screen &
super + n
    xdotool search --onlyvisible --classname scratchncmpcpp windowunmap || xdotool search --classname scratchncmpcpp windowmap || urxvtc_mod -name scratchncmpcpp -e ncmpcpp &
super + t
    xdotool search --onlyvisible --classname scratchrtorrent windowunmap || xdotool search --classname scratchrtorrent windowmap || urxvtc_mod -name scratchrtorrent -e rtorrent &
#super + i
#    xdotool search --onlyvisible --classname scratchweechat windowunmap || xdotool search --classname scratchweechat windowmap || urxvtc_mod -name scratchweechat -e weechat-curses &
super + i
    xdotool search --onlyvisible --classname scratchsshweechat windowunmap || xdotool search --classname scratchsshweechat windowmap || urxvtc_mod -name scratchsshweechat -e ssh -t nitelite weechat-screen &
super + o
    xdotool search --onlyvisible --classname scratchhtop windowunmap || xdotool search --classname scratchhtop windowmap || urxvtc_mod -name scratchhtop -e htop &
super + m
    xdotool search --onlyvisible --classname scratchmutt windowunmap || xdotool search --classname scratchmutt windowmap || urxvtc_mod -name scratchmutt -e mutt &
super + v
    xdotool search --onlyvisible --classname scratchsound windowunmap || xdotool search --classname scratchsound windowmap || urxvtc_mod -name scratchsound -e alsamixer &
super + r
    xdotool search --onlyvisible --classname scratchranger windowunmap || xdotool search --classname scratchranger windowmap || urxvtc_mod -name scratchranger -e ranger &
super + u
    xdotool search --onlyvisible --classname scratchnewsbeuter windowunmap || xdotool search --classname scratchnewsbeuter windowmap || urxvtc_mod -name scratchnewsbeuter -e newsbeuter &

#
# other
#

# Lock screen
super + x
  lock.sh

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd

XF86MonBrightnessDown
  xbacklight -dec 10
super + XF86MonBrightnessDown
  xbacklight -set 0
XF86MonBrightnessUp
  xbacklight -inc 10
super + XF86MonBrightnessUp
  xbacklight -set 100

# XF86TaskPane
#   /usr/local/bin/rotate
#
# XF86RotateWindows
#   /usr/local/bin/rotate

XF86TaskPane
  thinkpad-rotate

XF86RotateWindows
  thinkpad-rotate

