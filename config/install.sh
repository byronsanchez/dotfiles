#!/usr/bin/env bash

set -e

export DOTFILES="$HOME/.dotfiles"

# copy the dotfiles in config to the home directory. we don't symlink these
# since .config tends to have a lot of application data, caches, etc.
rsync -avz $DOTFILES/config/ $HOME/.config/

