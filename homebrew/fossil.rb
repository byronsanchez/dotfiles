require 'formula'

class Fossil < Formula
  homepage 'http://www.fossil-scm.org/'
  head 'fossil://http://www.fossil-scm.org/'
  url "http://www.fossil-scm.org/download/fossil-src-20140612172556.tar.gz"
  sha1 "173c3350ba39ecfee6e660f866b4f3104e351b33"
  version '1.29'

  option 'without-json', 'Build without "json" command support.'
  option 'without-tcl', "Build without the tcl-th1 command bridge."

  patch :p0, :DATA

  def install
    args = []
    args << "--json" if build.with? 'json'
    args << "--with-tcl" if build.with? 'tcl'

    system "./configure", *args
    system "make"
    bin.install 'fossil'
  end
end

__END__

--- src/export.c.orig	2014-06-12 21:16:19.653634646 -0400
+++ src/export.c	2014-06-13 02:05:55.478467295 -0400
@@ -29,6 +29,7 @@ static void print_person(const char *zUs
   const char *zContact;
   char *zName;
   char *zEmail;
+  int zName_length;
   int i, j;
 
   if( zUser==0 ){
@@ -72,6 +73,10 @@ static void print_person(const char *zUs
     ** Found beginning of email address. Look for the end and extract
     ** the part.
      */
+    // the length is not zero-indexed, so it will not count the angle bracket; 
+    // however we must still subtract 1 for the space that occurs before the 
+    // angle bracket
+    zName_length = i - 1;
     zEmail = mprintf("%s", &zContact[i]);
     for(i=0; zEmail[i] && zEmail[i]!='>'; i++){}
     if( zEmail[i]=='>' ) zEmail[i+1] = 0;
@@ -79,13 +84,18 @@ static void print_person(const char *zUs
     /*
     ** Found an end marker for email, but nothing else.
      */
+    // in this case, we want to use everything before the closed angle bracket 
+    // as the name. since length is not zero-indexed, the fact that the value of 
+    // the position IS zero-indexed means that the closed angle bracket will not 
+    // be part of the final substring and thus, we can use the i value directly.
+    zName_length = i;
     zEmail = mprintf("<%s>", zUser);
   }
   /*
   ** Here zContact[i] either '<' or '>'. Extract the string _before_
   ** either as user name.
   */
-  zName = mprintf("%.*s", i-1, zContact);
+  zName = mprintf("%.*s", zName_length, zContact);
   for(i=j=0; zName[i]; i++){
     if( zName[i]!='"' ) zName[j++] = zName[i];
   }
