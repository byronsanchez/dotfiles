# Install command-line tools using Homebrew
# Usage: `brew bundle Brewfile`

# Make sure we’re using the latest Homebrew
update

# Upgrade any already-installed formulae
upgrade

# Brew cask
tap phinze/homebrew-cask
install brew-cask

############
# OSX system

# Install GNU core utilities (those that come with OS X are outdated)
# Don’t forget to add `$(brew --prefix coreutils)/libexec/gnubin` to `$PATH`.
install coreutils
# Install GNU `find`, `locate`, `updatedb`, and `xargs`, `g`-prefixed
install findutils --default-names
# Install some other useful utilities like `sponge`
install moreutils
install binutils
install homebrew/dupes/diffutils
install homebrew/dupes/ed --default-names
install gawk
install gnu-indent --default-names
# Install GNU `sed`, overwriting the built-in `sed`
install gnu-sed --default-names
install gnu-tar --default-names
install gnu-which --default-names
install gnutls --default-names
install homebrew/dupes/grep --default-names
install gnu-getopt
install homebrew/dupes/gzip
install homebrew/dupes/unzip
install homebrew/dupes/screen
install watch
install wdiff --with-gettext
# Install wget with IRI support
install wget --enable-iri
# Install Bash 4
install bash
install homebrew/dupes/less
install homebrew/dupes/gdb
install homebrew/dupes/gpatch
install homebrew/dupes/m4
install homebrew/dupes/nano
install ack
install homebrew/dupes/apple-gcc42
install autoconf
install homebrew/dupes/make
install automake
install homebrew/dupes/openssh --with-brewed-openssl

######
# base

install ranger
install rsync
install vim --override-system-vi --with-lua --with-perl
cask install macvim --custom-system-icons --force
install pigz
install p7zip
install nmap
install curl
install openssl
install unrar
install htop-osx
install tree
install the_silver_searcher
install ssh-copy-id
install homebrew/dupes/tcpdump
install pv
install zsh
install zsh-completions

#####
# vcs

install fossil
install git --with-blk-sha1 --with-brewed-curl --with-brewed-openssl --with-pcre
install svn
install mercurial
install bzr
install rcs

#####
# vpn

install openvpn

#############
# development

install go
install python3
install node
install homebrew/versions/lua52
install python
install ruby

############
# dev-ops

cask install mysqlworkbench
install xctool
install homebrew/binary/packer
cask install virtualbox --force
cask install vagrant --force
install sqlite

#######
# media

cask install xld --force
cask install vlc --force
install ffmpeg --force
install mplayer

#############
# workstation

install grc
install lynx
install elinks
install weechat --with-aspell --with-perl --with-python
install terminal-notifier
cask install flux --force
cask install colors --force
cask install easysimbl --force
cask install akype --force
install imagemagick --with-webp
install aview
install fbida
install libcaca
install aalib
install optipng
install exiftool
cask install google-chrome --force
cask install firefox --force
cask install opera --force
install pwgen
install sf-pwgen
install dropbox
install libreoffice
install gimp
install inkscape
cask install teamviewer

# Remove outdated versions from the cellar
cleanup

