# GPG

A couple of notes regardinghow I implement GPG keys in my workflow. This is
based on the [method described
here](https://alexcabal.com/creating-the-perfect-gpg-keypair/).

This sort of parallels the PKI used in the nitelite.io network.

### keyring policy

GPG default = 1 signing subkey and 1 encryption subkey

The signing subkey is treated as the root "CA" and used to sign a new signing
subkey. This results in a total of 3 keys. The original signing subkey is then
stored in a safe location and removed from the workstation, leaving only the
encryption subkey and the new signing subkey.

These 2 keys are used normally. If the workstation is ever compromised, revoke
them and use the original "root" signing subkey to generate new keys.

There is a further addition.

Do this a second time on the semi-trusted nacgube for an entirely new keyring.
This will be used in semi-trusted contexts. Files encrypted to this key are only
for this context (eg I do not encrypt personal financial info to the
  semi-trusted context, only the credentials necessary)

On trusted workstation, import the public key of the semi-trusted encryption
key. Now encrypt to the appropriate identities for each file.

### Other notes

(more research and clarifaction needed for multiple encryption subkeys)

A new keyring is necessary for the second encryption key.

I could just create a new encryption subkey on the primary keyring, however,
encrypting for a particular subkey becomes problematic. For example, attempting
to use different encryption keys for different contexts is (as far as I am
aware) not possible.

Eg. lets say you want to have mail encrypted for the personal context associated
with the primary encryption subkey and mail encrypted for the semi-trusted
context with the secondary encryption subkey. Well there is no way to associate
uids with subkeys. There is no way to have the proper encryption subkey used for
the message. So when a sender encrypts a message to <primary keyring uid>, it'll
be encrypted to ONE of the encryption subkeys (depending on which public keys
they have through the keyservers). since there is only one primary uid, managing
multiple uids and associating them with encryption subkeys does not appear to be
possible.

So for now, it's two different keyrings- one for personal context and the other
for the semi-trusted context.

