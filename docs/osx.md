install xquartz: http://xquartz.macosforge.org/
install trashit: http://www.nonamescriptware.com/wp-content/uploads/Trashit.zip

also do this:

    brew edit mutt

in the options put:

    option "with-sidebar-patch", "Apply sidebar patch"

in the patches put:

    patch do
      url "https://raw.github.com/nedos/mutt-sidebar-patch/7ba0d8db829fe54c4940a7471ac2ebc2283ecb15/mutt-sidebar.patch"
      sha1 "1e151d4ff3ce83d635cf794acf0c781e1b748ff1"
    end if build.with? "sidebar-patch"

don't forget to:

    install nitelite.io certs
    place rsync_wrapper.sh in /usr/bin
    place validate-rsync.sh in ${HOME}


