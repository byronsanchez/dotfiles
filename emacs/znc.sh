#!/bin/sh
#
# ZNC.el has erratic behavior when connecting to multiple irc networks
# This patch fixes that.
#
# source: https://github.com/sshirokov/ZNC.el/issues/13

echo "Patching ZNC.el..."
cur_dir=`pwd`
cd "$HOME"/.emacs.d/elpa/znc-*
curl https://github.com/rschuetzler/ZNC.el/commit/e58db9afef82957ffcea6a72977f5f2b4fb53d05.patch | patch
# FIXME
#emacs --eval "(byte-recompile-directory $TARGET_DIR/\.emacs\.d/elpa/znc-*/ 0)"
cd $cur_dir
echo "Done patching ZNC.el."

