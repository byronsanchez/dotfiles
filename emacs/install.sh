#!/usr/bin/env bash

# Setup the backup org directories
mkdir -p $HOME/org
mkdir -p $HOME/org/blogs
mkdir -p $HOME/org/clients
mkdir -p $HOME/org/projects
mkdir -p $HOME/org/journals
mkdir -p $HOME/org/archives
mkdir -p $HOME/org/reference
mkdir -p $HOME/org/org
mkdir -p $HOME/org/notes
touch ${HOME}/org/org/capture.org
echo "#+FILETAGS: REFILE" > ${HOME}/org/org/capture.org

##
## install org-mode
##

if [ ! -d "$HOME/.elisp/org-mode" ];
then
  mkdir -p $HOME/.elisp
  git clone --recursive https://code.orgmode.org/bzg/org-mode.git "${ZDOTDIR:-$HOME}/.elisp/org-mode"
fi

##
## "update" org-mode (make sure the selected version is compiled)
##

cur_dir="`pwd`"
cd "$HOME/.elisp/org-mode"
#git checkout release_8.2.10
git checkout release_9.1.7
make uncompiled
cd $cur_dir

##
## install bbdb
##

if [ ! -d "$HOME/.elisp/bbdb" ];
then
  mkdir -p $HOME/.elisp

  # Easiest way to install. Compiling fails on windows.
  # An alternative approach is to use this method for windows and the repo 
  # method (commented out below) for nix variants.
  cur_dir="`pwd`"
  cd ~/.elisp
  curl -C - -L -O http://bbdb.sourceforge.net/bbdb-2.35.tar.gz
  tar -zxvf bbdb-2.35.tar.gz
  rm bbdb-2.35.tar.gz
  cd bbdb-2.35/lisp
  curl -C - -L -O http://bbdb.sourceforge.net/bbdb-autoloads.el
  cd $cur_dir

  #git clone --recursive git://git.savannah.nongnu.org/bbdb.git  
  #"${ZDOTDIR:-$HOME}/.elisp/bbdb"
fi

##
## install spacemacs
##

if [ ! -d "$HOME/.elisp/spacemacs" ];
then
  mkdir -p $HOME/.elisp
  git clone https://github.com/syl20bnr/spacemacs "${ZDOTDIR:-$HOME}/.elisp/spacemacs"
fi

##
## updates for spacemacs can be done through the interface
##

##
## install org-mode-doc
##

if [ ! -e "$HOME/.elisp/org-mode-doc" ];
then
  mkdir -p $HOME/.elisp
  git clone git://git.hackbytes.io/byronsanchez/org-mode-doc.git "${ZDOTDIR:-$HOME}/.elisp/org-mode-doc"
fi

##
## update org-mode-doc
##

cur_dir="`pwd`"
cd "$HOME/.elisp/org-mode-doc"
git pull
cd $cur_dir

##
## install org plugins
##

if [ ! -e "$HOME/.elisp/org-protocol-capture-html" ];
then
    mkdir -p $HOME/.elisp
    git clone https://github.com/alphapapa/org-protocol-capture-html "${ZDOTDIR:-$HOME}/.elisp/org-protocol-capture-html"
fi

##
## install + update ox-wintersmith
##

rm -rf $HOME/.elisp/lisp-links/ox-wintersmith
ox_target=$HOME/.elisp/lisp-links/ox-wintersmith
echo "ox_target: ${ox_target}"
mkdir -p "${ox_target}"
curl -C - -L -o "${ox_target}/ox-wintersmith.tar.gz" https://git.hackbytes.io/byronsanchez/ox-wintersmith/snapshot/ox-wintersmith-master.tar.gz
tar -C "${ox_target}" --strip-components=1 -zxvf "${ox_target}/ox-wintersmith.tar.gz" ox-wintersmith-master
rm ${ox_target}/ox-wintersmith.tar.gz

# Create a workstation-isolated cache dir
mkdir ~/.elisp/.cache
mkdir ~/.elisp/savefiles

# if [ ! -e "$HOME/memacs" ];
# then
#     cur_dir="`pwd`"
#     cd ~
#     pip install virtualenv
#     virtualenv memacs
#     cd memacs
#     . bin/activate
#     pip install memacs
#     cd $cur_dir
# fi

