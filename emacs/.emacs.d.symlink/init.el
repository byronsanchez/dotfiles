;; Byron's .emacs file
;;
;; Author: Byron Sanchez <byron@hackbytes.io>

;; Without this comment emacs25 adds (package-initialize) here
;; (package-initialize)

;; init speed boost by upping that gc-cons-threshold
(let ((gc-cons-threshold most-positive-fixnum))

  ;; Set initial width and height of window, since I like a bigger window
  (if (display-graphic-p)
      (progn
        (setq initial-frame-alist
              '(
                (width . 218) ; chars
                (height . 56) ; lines
                ;;
                ))

        (setq default-frame-alist
              '(
                (width . 218)
                (height . 56)
                ;;
                )))
    )

  (setq byte-compile-warnings nil)
  ;; We need to set the eyebrowse prefix before loading spacemacs. Otherwise, it
  ;; will override the ~C-c C-w~ refile keybinding for org-refile.
  (setq eyebrowse-keymap-prefix (kbd "C-c C-e"))

  ;; Set the elpa directory in the worksation-isolated ~/.elisp directory. Byte
  ;; compilations will occur in the elpa directory, so we need to make sure they
  ;; don't get synced via Dropbox across different workstations (eg. windows and
  ;; linux byte compilation errors where it will work for one OS but not the
  ;; other).
  (setq package-user-dir "~/.elisp/elpa/")
  (setq spacemacs-cache-directory "~/.elisp/.cache/")
  (setq user-emacs-directory "~/.elisp/savefiles/")

  ;; Load spacemacs as baseline default config
  (setq spacemacs-start-directory "~/.elisp/spacemacs/")
  (load-file (concat spacemacs-start-directory "init.el"))

  ;; Emacs and some plugins like to write custom variables in a file. This will
  ;; ensure that these custom variables are isolated to an external file so we
  ;; don't get vcs issues and keep this file clean!
  (setq custom-file "~/.emacs.d/custom.el")
  (load custom-file 'noerror)

  ;; if we have an encrypted secrets file, load it on up
  ;; load files from the dotfile path
  (defun load-config (file)
    (load-file (concat user-emacs-directory file)))

  (if (file-exists-p (concat user-emacs-directory "secrets.el"))
      (load-config "secrets.el"))
  (if (file-exists-p (concat user-emacs-directory "secrets.el.gpg"))
      (load-config "secrets.el.gpg"))

  ;; Setup cygwin for windows workstations
  ;;
  ;; Make cygwin binaries available to emacs
  (if (file-directory-p "c:/tools/cygwin/bin")
      (progn

        ;; Sets your shell to use cygwin's bash, if Emacs finds it's running
        ;; under Windows and c:\cygwin exists. Assumes that C:\cygwin\bin is
        ;; not already in your Windows Path (it generally should not be).
        ;;
        (let* ((cygwin-root "c:/tools/cygwin")
               (cygwin-bin (concat cygwin-root "/bin")))
          (when (and (eq 'windows-nt system-type)
  	                 (file-readable-p cygwin-root))

            ;; (add-to-list 'exec-path "c:/tools/cygwin/bin")
            (setq exec-path (cons cygwin-bin exec-path))
            (setenv "PATH" (concat cygwin-bin ";" (getenv "PATH")))

            ;; By default use the Windows HOME.
            ;; Otherwise, uncomment below to set a HOME
            ;;      (setenv "HOME" (concat cygwin-root "/home/eric"))

            ;; NT-emacs assumes a Windows shell. Change to bash.
            ;; windows has a 'native' bash, so try that before cygwin bash
            (setq shell-file-name "bash")
            (setq explicit-bash-args '("--noediting" "--login" "-i"))
            (setenv "SHELL" shell-file-name)
                                        ; (setq explicit-shell-file-name "C:/tools/cygwin/bin/bash.exe")
                                        ; (setq explicit-bash.exe-args '("--noediting" "--login" "-i"))
                                        ; (setq explicit-shell-file-name shell-file-name)

            ;; This removes unsightly ^M characters that would otherwise
            ;; appear in the output of java applications.
            (add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)))))

  (require 'tramp)

  ;; set the default tramp connection method
  ;; (cond  ((eq window-system 'w32)
  ;;         (setq tramp-default-method "scpx"))
  ;;        (t
  ;;         (setq tramp-default-method "scp")))

  ;; at-pl for windows, scp for linux/osx
  (cond  ((eq window-system 'w32)
          (setq tramp-default-method "at-pl"))
         (t
          (setq tramp-default-method "scp")))

  (if (eq window-system 'w32)
      (progn
        (setenv "GIT_SSH" "C:/Program Files/PuTTY/plink.exe")

        ;; Load a custom version of tramp so we can avoid bugs with plink for using
        ;; tramp on windows boxes
        ;; (setenv "PATH" (concat "C:/Program Files (x86)/PuTTY;" (getenv "PATH")))
        ;; (add-to-list 'Info-default-directory-list (expand-file-name "~/.elisp/tramp-2.3.1/lisp"))

        ;; Define a new tramp method name to avoid the conflict of
        ;; default version of 'plink'

        ;; For find-file (workstation-notebook-prefix)
        (add-to-list 'tramp-methods
                     (list "at-pl"
                           '(tramp-login-program "plink")
                           (cons 'tramp-login-args
                                 (list (list
                                        '("-l" "%u")
                                        '("-P" "%p")
                                        ;; '("-ssh")
                                        '("-t")
                                        '("-A")
                                        ;; x-forwarding
                                        ;;'("-x")
                                        ;;
                                        ;; keyfile
                                        ;;
                                        ;; (if (equal system-type 'windows-nt)
                                        ;;     (progn
                                        ;;       (setq keyfilename (expand-file-name (concat (getenv "HOME") "/.ssh/osx_rsa_ppk.ppk")))
                                        ;;       (if (file-exists-p keyfilename)
                                        ;;           (list "-i" (concat "\"" keyfilename "\"")))))
                                        '("%h")
                                        '("\"")
                                        '("env 'TERM=dumb' 'PROMPT_COMMAND=' 'PS1=#$ '")
                                        '("/bin/sh")
                                        '("\"")
                                        )))
                           '(tramp-remote-shell "/bin/sh")
                           '(tramp-remote-shell-login ("-l"))
                           '(tramp-remote-shell-args ("-c"))
                           '(tramp-default-port 22))
                     t)

        ;; For eshell ssh

        ;; (add-to-list 'tramp-methods
        ;;              (list "at-sh"
        ;;                    '(tramp-login-program "plink")
        ;;                    (cons 'tramp-login-args
        ;;                          (list (list
        ;;                                 '("-l" "%u")
        ;;                                 '("-P" "%p")
        ;;                                 ;; '("-ssh")
        ;;                                 '("-t")
        ;;                                 '("-D")
        ;;                                 ;; x-forwarding
        ;;                                 ;;'("-x")
        ;;                                 (if (equal system-type 'windows-nt)
        ;;                                     (progn
        ;;                                       (setq keyfilename (expand-file-name (concat (getenv "HOME") "/.ssh/osx_rsa_ppk.ppk")))
        ;;                                       (if (file-exists-p keyfilename)
        ;;                                           (list "-i" (concat "\"" keyfilename "\"")))))
        ;;                                 '("%h")
        ;;                                 ;; '("\"")
        ;;                                 ;; '("env 'PROMPT_COMMAND=' 'PS1=#$ '")
        ;;                                 ;; '("/bin/sh")
        ;;                                 ;; '("\"")
        ;;                                 )))
        ;;                    '(tramp-remote-shell "/bin/bash")
        ;;                    '(tramp-remote-shell-login ("-l"))
        ;;                    '(tramp-remote-shell-args ("-c"))
        ;;                    '(tramp-default-port 22))
        ;;              t)
        ))

  ;; tramp backup path (if not set, save in local backup directory)
  (setq tramp-backup-directory-alist nil)
  (setq tramp-auto-save-directory nil)

  ;; (setq tramp-copy-size-limit nil)
  ;; (setq tramp-verbose 6)

  ;; load all the scripts in local site-lisp
  (eval-when-compile (require 'cl))
  (if (fboundp 'normal-top-level-add-to-load-path)
      (let* ((my-lisp-dir "~/.emacs.d/site-lisp/")
             (default-directory my-lisp-dir))
        (progn
          (setq load-path
                (append
                 (loop for dir in (directory-files my-lisp-dir)
                       unless (string-match "^\\." dir)
                       collecting (expand-file-name dir))
                 load-path)))))

  ;; Set up lisp load paths
  (add-to-list 'load-path (expand-file-name "~/.elisp/bbdb-2.35/lisp"))
  (add-to-list 'load-path (expand-file-name "~/.elisp/lisp"))
  (add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))
  (add-to-list 'load-path (expand-file-name "~/.emacs.d/site-lisp"))
  (add-to-list 'load-path (expand-file-name "~/.elisp/lisp-links/ox-wintersmith/"))

;;;
;;; Package Management
;;;

  ;; package signatures sometimes cause breakages on legitimate packages
  ;;
  ;; source:
  ;; http://emacs.stackexchange.com/questions/233/how-to-proceed-on-package-el-signature-check-failure
  (setq package-check-signature nil)

  (setq package-enable-at-startup nil)

  (setq package-archives '(
                           ("melpa" . "http://melpa.milkbox.net/packages/")
                           ("gnu" . "http://elpa.gnu.org/packages/")))

  ;; Define package repositories
  (add-to-list 'package-archives
               '("marmalade" . "http://marmalade-repo.org/packages/"))
  (add-to-list 'package-archives
               '("melpa" . "http://melpa.org/packages/") t)
  (add-to-list 'package-archives
               '("org" . "http://orgmode.org/elpa/") t)
  (add-to-list 'package-archives
               '("elpy" . "https://jorgenschaefer.github.io/packages/") t)

  ;; Check if we're on Emacs 24.4 or newer, if so, use the pinned package feature
  ;; We pin packages from certain repos.
  (when (boundp 'package-pinned-packages)
    (setq package-pinned-packages
          '(
            ;; "stable" packages
            (zenburn-theme      . "melpa-stable")
            (anti-zenburn-theme . "melpa-stable")
            (zen-and-art-theme  . "marmalade")
            (arjen-grey-theme . "melpa-stable")
            (magit . "melpa")
            ;; "unstable" package
            )))

  ;; Now that package management configuration is ready, we can load the
  ;; packages.
  (package-initialize)

  ;; Bootstrap 'use-package'
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))

  (setq use-package-verbose t)
  ;; (setq use-package-always-ensure t)


  ;; setup-cygwin for paths
  (setq *win32* (eq system-type 'windows-nt) )
  ;; win32 auto configuration, assuming that cygwin is installed at "c:/cygwin"
  (if *win32*
      (progn
        
        (use-package cygwin-mount :ensure t)
        (setq cygwin-root-directory "c:/tools/cygwin")
        (setq cygwin-mount-cygwin-bin-directory "c:/tools/cygwin/bin")
        (use-package setup-cygwin :ensure t)
        (require 'setup-cygwin)
                                        ;(setenv "HOME" "c:/tools/cygwin/home/someuser") ;;
                                        ;better to set HOME env in GUI
        ;; setup any other env var you need through user env vars for native windows as well
        ))

  ;; Load the my own custom configs
  (package-initialize)

;;;
;;; Org Mode
;;;

  (add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode))

  ;; Associate major modes with the specified file patterns; in this case,
  ;; associate org mode with .org files and txt files

  ;; The norang org-mode document provides variable overrides so we can define our
  ;; own custom paths, options, etc.
  ;;
  ;; We define these variables here prior to loading the norang org-mode-doc.

  (setq org-mode-user-lisp-path "~/.elisp/org-mode/lisp")

  ;; Our public, non-encrypted capture file.
  ;;
  ;; This file is used as the default capture and agenda file when we don't have
  ;; access to the main notebook, either due to encryption volumes not being
  ;; mounted or no access to a remote server with valid volumes mounted.
  ;;
  ;; This file will always remain as part of the agenda, even if valid volumes are
  ;; found. However, it is not versioned. This is only meant for quick captures
  ;; and during initial start up of emacs (eg. daemon on X-server without
  ;; encrypted volumes mounted)! It's a quick capture in case there's no time for
  ;; whatever reason to load up the real notebook.
  ;;
  ;; When the real notebook is accessible and available, the "real" versioned
  ;; capture file takes precedence and replaces this default one as the capture
  ;; file. Both will be in the agenda file-list to allow for refiling and such.
  (setq workstation-notebook-prefix "~/org")
  (setq nl-file-prefix "~")
  (setq org-user-agenda-files (quote ("~/org/org/capture.org")))
  (setq org-mode-user-contrib-lisp-path "~/.elisp/org-mode/contrib/lisp")

  ;; We NEED to load our custom org-mode implementation here. That way, the
  ;; org-babel that gets invoked is the right version that we have checked out of
  ;; the git repository. If it is not the right version, it WILL PRODUCE ERRORS!
  ;; We define the variables earlier to satisfy the org-mode-doc expectations
  ;; without having to minimize our modifications.
  (add-to-list 'load-path (expand-file-name "~/.elisp/org-mode/lisp"))
  (add-to-list 'load-path (expand-file-name "~/.elisp/org-mode/contrib/lisp"))

  (add-to-list 'load-path (expand-file-name "~/.elisp/org-protocol-capture-html"))

  ;; Now that we have the correct version of org-mode path loaded, we can invoke
  ;; org-babel.
  (org-babel-load-file "~/.elisp/org-mode-doc/org-mode.org")
  (org-babel-load-file "~/.emacs.d/config.org")

  ;; Spacemacs brings the garbage collection threshold way up to 100MB. This
  ;; produces an annoying effect where gc occurs frequently (on windows, approx
  ;; every 30 seconds or so of editing activity) and locks the UI in an annoying
  ;; way (like 2-3 seconds of frequent UI locking)
  ;;
  ;; Thus, we bring it back down to it's default value of 800KB or so
  ;;
  ;; TODO: Test out 100MB on non-windows systems and see if the locking effect is
  ;; also noticeable.
  ;; (setq gc-cons-threshold 800000)
  ;; (setq gc-cons-threshold (* 800 1024))

  (if *win32*
      (progn
        ;;(setq gc-cons-threshold 50000000)
        ;; trying 100mb to see how it impacts stuff
        ;; (setq gc-cons-threshold 100000000)

        ;; (setq gc-cons-threshold (* 511 1024 1024))
        ;; (setq gc-cons-percentage 0.5)
        ;; (run-with-idle-timer 20 t #'garbage-collect)

        (setq garbage-collection-messages t)
        ;; (run-with-idle-timer 20 t #'garbage-collect)

        ;; source: https://bling.github.io/blog/2016/01/18/why-are-you-changing-gc-cons-threshold/
        ;;
        ;; Only gc during intensive tasks
        (defun my-minibuffer-setup-hook ()
          (setq gc-cons-threshold most-positive-fixnum))

        (defun my-minibuffer-exit-hook ()
          ;;(setq gc-cons-threshold 800000)
          (setq gc-cons-threshold 800000)
          (setq gc-cons-threshold (* 511 1024 1024))
          (setq gc-cons-threshold (* 1024 1024 1024))
          (setq gc-cons-percentage 0.5)
          )

        (add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
        (add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook))))

(message "finished loading org files")
(message "setting gc-cons-threshold back to lower value")

;; Don't byte compile this config file:

;;; Local Variables:
;;; no-byte-compile: t
;;; End:
