;;; nitelite-theme.el --- Custom face theme for Emacs  -*-coding: utf-8 -*-

;; Copyright (C) 2011-2012 Free Software Foundation, Inc.

;; Author: Kristoffer Grönlund <krig@koru.se>

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(deftheme nitelite
  "Medium-contrast faces with a dark gray background.
Adapted, with permission, from a Vim color scheme by Lars H. Nielsen.
Basic, Font Lock, Isearch, Gnus, Message, and Ansi-Color faces
are included.")

(let ((class '((class color) (min-colors 89))))
  (custom-theme-set-faces
   'nitelite

   ;; set a dark bg for gui, since we can use the wm to make it transparent
   ;; otherwise, set to nil for term so it won't set any bg and the term can set 
   ;; the bg (like transparency)
   ;;
   ;; setting nil for gui results in a white bg for gui
   ;; setting a bg color for term results in that color taking over with no 
   ;; possibility of transparency. It'll overlay the term transparency
   (if window-system
   `(default ((,class (:background "#000000" :foreground "#d0d0d0"))))
   `(default ((,class (:background nil :foreground "#d0d0d0"))))
     )

   ;; DONE

   `(cursor ((,class (:background "#d7ff87" :weight bold))))

	 ;; old-1
   ;`(font-lock-function-name-face ((,class (:foreground "#d7ff87" :weight 
   ;bold))))
   ;`(font-lock-variable-name-face ((,class (:foreground "#d7ff87" :weight 
   ;bold))))

   ;; Font lock faces
   `(font-lock-builtin-face ((,class (:foreground "#d7ff87"))))
   `(font-lock-function-name-face ((,class (:foreground "#afffff" :weight bold))))
   `(font-lock-variable-name-face ((,class (:foreground "#d0d0d0" :weight bold))))
   `(font-lock-constant-face ((,class (:foreground "#ffd75f" :weight bold))))
   `(font-lock-keyword-face ((,class (:foreground "#afffff" :weight bold))))
   `(font-lock-string-face ((,class (:foreground "#d7ff87"))))
   `(font-lock-preprocessor-face ((,class (:foreground "#ffd75f" :weight bold))))
   `(font-lock-type-face ((,class (:foreground "#ffffaf"))))
   `(font-lock-comment-face ((,class (:foreground "#808080" :weight bold))))

   ;; Highlighting faces
   `(region ((,class (:background "#b2b2b2" :foreground "#1c1c1c" :weight bold))))
   `(fringe ((,class (:background "#444444"))))
   `(highlight ((,class (:background "#262626"))))
   `(secondary-selection ((,class (:background "#333366" :foreground "#f6f3e8"))))
   `(isearch ((,class (:background "#626262" :foreground "#d787ff" :weight bold))))
   ; inactive search
   `(lazy-highlight ((,class (:background "#626262" :foreground "#d787ff" :weight bold))))

   ;; Escape and prompt faces
   `(minibuffer-prompt ((,class (:foreground "green"))))

   ;; Button and link faces
   ;; TODO: Consider using keyword face and type face
   `(link ((,class (:foreground "#8ac6f2" :underline t))))
   `(link-visited ((,class (:foreground "#e5786d" :underline t))))

   ;; Mode line faces
   `(mode-line ((,class (:background "#666666" :foreground "#d0d0d0"))))
   `(mode-line-inactive ((,class (:background "#353535" :foreground "#666666"))))

   ;; INPROGRESS
   ;; TODO: These haven't been updated to "nitelite" styles

   ;; Escape and prompt faces
   `(escape-glyph ((,class (:foreground "#ff0000" :weight bold))))

   ;; Button and link faces
   `(button ((,class (:background "#ff0000" :foreground "#d0d0d0"))))
   `(header-line ((,class (:background "#303030" :foreground "#e7f6da"))))
   ;; emacs-only
   `(font-lock-warning-face ((,class (:foreground "#ccaa8f"))))

   ))

(custom-theme-set-variables
 'nitelite
 '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682"
			    "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"]))

;; TODO: Set mail faces

; (custom-set-faces
;  ;; custom-set-faces was added by Custom.
;  ;; If you edit it by hand, you could mess it up, so be careful.
;  ;; Your init file should contain only one such instance.
;  ;; If there is more than one, they won't work right.
;  '(message-cited-text ((((class color)) (:foreground "Blue"))))
;  '(message-header-cc ((((class color)) (:bold t :foreground "green2"))))
;  '(message-header-name ((((class color)) (:bold nil :foreground "Blue"))))
;  '(message-header-other ((((class color)) (:foreground "Firebrick"))))
;  '(message-header-xheader ((((class color)) (:foreground "Blue"))))
;  '(message-mml ((((class color)) (:foreground "DarkGreen"))))
;  '(message-separator ((((class color)) (:foreground "Tan")))))

; TODO: Set org-mode faces

; (custom-set-faces
;  ;; custom-set-faces was added by Custom.
;  ;; If you edit it by hand, you could mess it up, so be careful.
;  ;; Your init file should contain only one such instance.
;  ;; If there is more than one, they won't work right.
;  '(org-agenda-done ((t (:foreground "ForestGreen"))))
;  '(org-done ((t (:foreground "ForestGreen" :weight bold))))
;  '(org-habit-alert-face ((((background light)) (:background "#f5f946"))))
;  '(org-habit-alert-future-face ((((background light)) (:background "#fafca9"))))
;  '(org-habit-clear-face ((((background light)) (:background "#8270f9"))))
;  '(org-habit-clear-future-face ((((background light)) (:background "#d6e4fc"))))
;  '(org-habit-overdue-face ((((background light)) (:background "#f9372d"))))
;  '(org-habit-overdue-future-face ((((background light)) (:background "#fc9590"))))
;  '(org-habit-ready-face ((((background light)) (:background "#4df946"))))
;  '(org-habit-ready-future-face ((((background light)) (:background "#acfca9"))))
;  '(org-headline-done ((t nil)))
;  '(org-scheduled ((((class color) (min-colors 88) (background light)) nil)))
;  '(org-upcoming-deadline ((((class color) (min-colors 88) (background light)) (:foreground "Brown")))))

;; TODO: Customize colors for calfw

;; (custom-set-faces
;;  '(cfw:face-title ((t (:foreground "#f0dfaf" :weight bold :height 2.0 :inherit variable-pitch))))
;;  '(cfw:face-header ((t (:foreground "#d0bf8f" :weight bold))))
;;  '(cfw:face-sunday ((t :foreground "#cc9393" :background "grey10" :weight bold)))
;;  '(cfw:face-saturday ((t :foreground "#8cd0d3" :background "grey10" :weight bold)))
;;  '(cfw:face-holiday ((t :background "grey10" :foreground "#8c5353" :weight bold)))
;;  '(cfw:face-grid ((t :foreground "DarkGrey")))
;;  '(cfw:face-default-content ((t :foreground "#bfebbf")))
;;  '(cfw:face-periods ((t :foreground "cyan")))
;;  '(cfw:face-day-title ((t :background "grey10")))
;;  '(cfw:face-default-day ((t :weight bold :inherit cfw:face-day-title)))
;;  '(cfw:face-annotation ((t :foreground "RosyBrown" :inherit cfw:face-day-title)))
;;  '(cfw:face-disable ((t :foreground "DarkGray" :inherit cfw:face-day-title)))
;;  '(cfw:face-today-title ((t :background "#7f9f7f" :weight bold)))
;;  '(cfw:face-today ((t :background: "grey10" :weight bold)))
;;  '(cfw:face-select ((t :background "#2f2f2f")))
;;  '(cfw:face-toolbar ((t :foreground "Steelblue4" :background "Steelblue4")))
;;  '(cfw:face-toolbar-button-off ((t :foreground "Gray10" :weight bold)))
;;  '(cfw:face-toolbar-button-on ((t :foreground "Gray50" :weight bold))))

(provide-theme 'nitelite)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; nitelite-theme.el ends here
