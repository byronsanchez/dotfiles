#! /usr/bin/env python2
from subprocess import check_output
import os
import argparse

def get_pass(email_address):
    secret_dir = os.environ.get('SECRETDIR') + "/files"
    if secret_dir:
        return check_output("gpg2 --use-agent --quiet --batch -d " + secret_dir + "/"  + email_address + ".gpg", shell=True).strip("\n")
    else:
        return ""

 # Propagate gnus-expire flag
# TODO: Figure out if this is useful for gnus
import offlineimap.imaputil as IU
if not hasattr(IU, 'monkeypatchdone'):
    IU.flagmap += [('gnus-expire','E'),
                   ('gnus-dormant', 'Q'),
                   ('gnus-save', 'V'),
                   ('gnus-forward', 'W')]
    IU.monkeypatchdone = True

if __name__ == '__main__':
    SERVICE = 'mbsync'

    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--get', '-g', type=str, help='Account to get password')

    args = parser.parse_args()

    if args.get:
        print(get_pass(args.get))

