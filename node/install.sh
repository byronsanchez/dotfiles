#!/bin/sh

# for web apps
npm install -g @angular/cli
npm install -g typescript
npm install -g tslint
npm install -g rxjs-tslint
npm install -g postcss-cli
# for knex db migrations
npm install -g knex
# for unit testing
npm install -g mocha
npm install -g chai

# for blogs
npm install -g coffeescript
npm install -g wintersmith
# used by ox-wintersmith for higlighting org code-blocks
npm install -g highlight.js
#npm install -g highlight.js-cli
# a modified version introduces custom functionality not published in the 
# authoritative repo
npm install -g ~/Dropbox/hackBytes/Projects/byronsanchez/highlight.js-cli

# Tools emacs can hook into for front-end development
# auto-completion and documentation features
npm install -g tern
# formatting features
npm install -g js-beautify

