#!/bin/sh

# This cygwin script is invoked through a powershell script. Because of this, /usr/bin is not set in PATH and many
# applications are not accessible. So we set it here.
PATH=/usr/bin:$PATH;
export PATH;

source ~/projects/byronsanchez/dotfiles/scripts/helpers/common.lib.sh

curl -O -J -L -k rawgit.com/transcode-open/apt-cyg/master/apt-cyg > apt-cyg
install apt-cyg /bin

if stringContain "MSYS" "$(uname -s)" || stringContain "CYGWIN" "$(uname -s)" || stringContain "MINGW" "$(uname -s)";
then
  apt-cyg install aspell
  apt-cyg install aspell-en
  apt-cyg install autoconf
  apt-cyg install bzip2
  apt-cyg install ca-certificates
  # we were using the windows version earlier; we want the cygwin version in 
  # cygwin to avoid ssl insecure errors
  apt-cyg install curl
  apt-cyg install cvs
  # NOTE: Disabling fossil until version 2 or higher is available. Version 2 is 
  # the one I'm using for my repositories.
  #apt-cyg install fossil
  apt-cyg install fortune-mod
  apt-cyg install git
  apt-cyg install gnome-keyring
  # NOTE: Use GPG4Win to get gpg-agent functionality! It works fine with 
  # vim-gnupg and encfs mounting aliases
  # apt-cyg install gnupg
  # Also make sure it's removed from Cygwin since sometimes it may get installed 
  # by default (eg. Cygwin updates via Chocolatey)
  apt-cyg remove gnupg
  apt-cyg install hostname
  apt-cyg install gzip
  apt-cyg install lua
  apt-cyg install msmtp
  apt-cyg install mutt
  apt-cyg install ncurses
  apt-cyg install offlineimap
  apt-cyg install optipng
  apt-cyg install ping
  apt-cyg install rsync
  apt-cyg install cygrunsrv
  apt-cyg install screen
  apt-cyg install sqlite3
  apt-cyg install tar
  apt-cyg install texlive
  apt-cyg install the_silver_searcher
  apt-cyg install tree
  apt-cyg install unzip
  apt-cyg install w3m
  apt-cyg install weechat
  apt-cyg install wget
  apt-cyg install which
  apt-cyg install whois
  apt-cyg install vim
  apt-cyg install zsh

  # needed to ensure git works properly without erroring out when cloning 
  # submodules (this will happen for some versions of git!!!)
  apt-cyg install gettext

  # needed to ensure emacs tramp can be custom installed properly (texinfo 
  # provides makeinfo, which tramp compilation requires)
  apt-cyg install texinfo
  apt-cyg install make

  # dependencies for pdf-tools on windows
  apt-cyg install libpoppler-devel
  apt-cyg install libpoppler-glib-devel
  apt-cyg install libpng-devel
  apt-cyg install make
  apt-cyg install gcc-core
  apt-cyg install gcc-g++
  apt-cyg install autoconf
  apt-cyg install automake
  apt-cyg install perl
  #emacs-w32

  # mysql client for emacs sqli integration
  apt-cyg install mysql
  apt-cyg install pwgen
  apt-cyg install ssh-pageant
  apt-cyg install pass

  # qpdf can be used for command-line pdf operations like decrypting a pdf with 
  # security rights
  apt-cyg install qpdf

  # required so emacs' projectile can index with the faster alien method on
  # Windows
  apt-cyg install dos2unix

  # used for generating embedded documentation in fossil repos
  apt-cyg install tcl
  ln -s /usr/bin/tclsh8.6.exe /usr/bin/tclsh
  
  # psql is useful to use along with an "IDE" for databases; sometimes you need 
  # more "surgery" than the IDE offers.
  apt-cyg install postgresql-client

  msmtp-config
  ssh-host-config
fi


