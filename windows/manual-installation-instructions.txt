Go to Windows Store -> Your Account -> Your Apps and download them all.

Get these apps from the Windows Store:

- yubikey-piv-manager (not available in chocolatey) - 
  https://developers.yubico.com/yubikey-piv-manager/Releases/

If you have trouble getting the smartcard to be recognized by gpg (gpg --card-status), remember to disable any other smart cards in the device manager.

As for the reg files, use BOTH of them to set up Microsoft Eva (Cortana text-to-speech) if Microsoft doesn't ship by default yet. Instructions:

https://superuser.com/questions/1141840/how-to-enable-microsoft-eva-cortanas-voice-on-windows-10

- Install and setup one drive in the projects/byronsanchez/documents folder so 
  it also gets synced through dropbox

# Fixing up emacs keybindings

- Disable windows intel graphics display hot keys to have access to paredit's C-M-Left+Right for slurping and barfing
- Disable Control Panel -> Language -> Advanced Settings -> Change language bar hotkeys ->  Change key sequence -> disable both hotkeys to have access to C-) for paredit slurping and barfing