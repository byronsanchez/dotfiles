﻿	
# Allow powershell script execution
Set-ExecutionPolicy Bypass

# Install Chocolatey
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install Windows packages via Chocolatey
choco install --yes --allow-empty-checksums $HOME\projects\byronsanchez\dotfiles\windows\packages.config

# Set HOME as a user-level environment variable for Cygwin.
#
# Cygwin typically sets it's user home directory in a custom cygwin-location (eg. C:\tools\cygwin\home).
# By setting this environment variable in the Windows user environment, Cygwin picks it up and interprets it as a request to have the cygwin home directory
# set to whatever path we specify. In this case, we want Cygwin to consider the Windows home directory as the Cygwin home directory as well!
[Environment]::SetEnvironmentVariable("HOME", "C:\Users\bfs50", "User")

# Set environment variables to integrate Emacs with Windows Explorer.
#
# This will add a context menu item that allows you to open text files with an emacs instance.
[Environment]::SetEnvironmentVariable("ALTERNATE_EDITOR", "C:\emacs\bin\runemacs.exe", "User")
# Use the workstation-isolated server file now
[Environment]::SetEnvironmentVariable("EMACS_SERVER_FILE", "C:\Users\bfs50\.elisp\savefiles\server\server", "User")

# Set SECRETDIR as a user-level environment variable for vim password management. This is for my custom use in case I need it.
[Environment]::SetEnvironmentVariable("SECRETDIR", "C:\Users\bfs50\.secrets\nix", "User")

# Set an environment variable so that Cygwin uses native window symlinks.
# 
# We also set this in the .zprofile dotfile, but upon provisioning of a new windows system, we need the variable to be set
# from Windows itself. That way windows picks it up when bootstrapping all the dotfiles. Otherwise, it would bootstrap the dotfiles
# with Cygwin-default symlinks, which are just Cygwin-specific cookie files that tell Cygwin to treat it like a symlink. But Windows
# File Explorer wouldn't know what to do with it.
[Environment]::SetEnvironmentVariable("CYGWIN", "winsymlinks:nativestrict", "User")

# TODO: Append the following directories to PATH - C:\Users\bfs50\.dotfiles\bin, bin-links, C:\tools\bin, C:\Program Files\Dokan\Dokan Library-1.0.1 (for encfs dismounting; this gets
# installed as a dep for encfs4win, but only encfs gets added to PATH automatically and dokan is left as an un-pathed lib dep), C:\Program Files (x86)\Yubico\YubiKey PIV Manager, C:\Program Files (x86)\GnuWin32\bin (mobileorg sha sums), C:\Program Files\VideoLAN\VLC
#
# C:\Users\bfs50\.nodejs for node global bins

# TODO: Install windows exes from applications directory into C:\tools\bin (eg. call-hotkey, paste, etc.)

# Install all Cygwin packages
& C:\tools\cygwin\bin\bash.exe -c "~/projects/byronsanchez/dotfiles/windows/cygwin.sh"

# Setup autohotkey to run on windows start-up
cmd /c mklink "C:\Users\bfs50\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\autohotkeys.ahk" "C:\Users\bfs50\Dropbox\hackBytes\Projects\byronsanchez\dotfiles\ahk\autohotkeys.ahk"

# TODO: Setup gpg for ssh authentication to be usable as soon as windows starts
# cmd /c mklink "C:\Users\bfs50\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\gpg-connect-agent-bye.exe" "\"C:\Program Files (x86)\GNU\GnuPG\gpg-connect-agent.exe\" /bye"

# Setup emacs symlink so that emacs configs can find the executables if necessary
#
# Chocolatey installs it in an unusual directory, so we just build a symlink in the root drive for easy access via scripts, configs, etc.
cmd /c mklink /D "C:\emacs" "C:\ProgramData\chocolatey\lib\emacs64\tools\emacs"
#  cmd /c mklink /D "C:\emacs" "C:\emax64\"

# Setup a cygwin link from root drive to Chocolatey. It seems there may be some hard-coded paths somewhere, but it's not in my config files. Perhaps it's one of the emacs packages
# In any case, the result that if the cygwin directory is inaccessible from C:\cygwin, then org-id generation fails
cmd /c mklink /D "C:\cygwin" "C:\tools\cygwin"

# Make the documents directory accessible to OneDrive
#
# This approach ensures that DropBox doesn't get stuck syncing forever due to OneDrive lock files (my original approach was to just specify a directory from the Dropbox folder tree as the root OneDrive directory, which caused those lock file issues). This new approach keeps the OneDrive root directory independently from Dropbox, but gives access to a Dropbox directory to OneDrive
# UPDATE 2018-03-18: I don't use this anymore since I write spreadsheets anywhere, like in org-mode directory structure
#cmd /c mklink /D "C:\Users\bfs50\OneDrive - Byron Sanchez\documents" C:\Users\bfs50\documents

# install build tools for windows so node works properly
npm install -g windows-build-tools

# TODO: Consider installing fossil here if Cygwin or Chocolatey package versions tend to be unreliable in the future

# Enable Hyper-V
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All

