#!/bin/sh

SRC_URI="https://github.com/simar7/iTerm2.git"
APP_DIR="${HOME}/applications"

if [ "$(uname -s)" == "Darwin" ];
then

  if [ ! -d "$DESTINATION/.git" ];
  then
    git clone -q ${SRC_URI} ${DESTINATION}
  else
    cur_dir="`pwd`"
    cd "$DESTINATION"
    git pull -q 
    cd "$cur_dir"
  fi

  if [ ! -e "${APP_DIR}/iTerm.app" ];
  then
    ln -s "${DESTINATION}/build/Development/iTerm.app" "${APP_DIR}/iTerm.app"
  fi

fi

