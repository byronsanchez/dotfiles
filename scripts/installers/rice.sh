#!/bin/sh

SRC_URI="https://github.com/janbrennen/rice.git"

if [ ! -d "$DESTINATION/.git" ];
then
  git clone -q ${SRC_URI} ${DESTINATION}
else
  cur_dir="`pwd`"
  cd "$DESTINATION"
  git pull -q 
  cd "$cur_dir"
fi

files="256color.pl
3spooky.lua
hack.exe
hack.exe.c
hackertyper.sh
pipes.sh"

# filenames may have spaces
IFS="
"

for f in $files;
do

  if [ ! -e "${BIN_DIR}/${f}" ];
  then
    ln -s ${DESTINATION}/${f} ${BIN_DIR}/${f}
  fi

done

