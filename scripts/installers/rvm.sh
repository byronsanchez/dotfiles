#!/bin/sh

if [ ! -e "${HOME}/.rvm" ];
then
  cur_dir="`pwd`"
  cd $HOME
  gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

  #curl -sSL https://get.rvm.io | bash -s stable -- --ignore-dotfiles
  curl -sSL https://get.rvm.io | bash -s stable --ignore-dotfiles
  cd $cur_dir
fi

