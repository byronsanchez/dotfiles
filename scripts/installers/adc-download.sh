#!/bin/sh

SRC_URI="https://github.com/michaeltyson/adc-download.git"

if [ "$(uname -s)" == "Darwin" ];
then

  if [ ! -d "$DESTINATION/.git" ];
  then
    git clone -q ${SRC_URI} ${DESTINATION}
  else
    cur_dir="`pwd`"
    cd "$DESTINATION"
    git pull -q 
    cd "$cur_dir"
  fi

# filenames may have spaces
  IFS="
"

  for f in ${DESTINATION}/*.sh;
  do

    chmod 0755 $f
    file=${f##*/}

    if [ ! -e "${BIN_DIR}/${file}" ];
    then
      ln -s ${f} ${BIN_DIR}/${file}
    fi

  done

fi

