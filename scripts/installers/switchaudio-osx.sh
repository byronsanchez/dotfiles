#!/bin/sh

SRC_URI="https://github.com/deweller/switchaudio-osx.git"
TARGET_FILE="${DESTINATION}/build/Release/SwitchAudioSource"

if [ "$(uname -s)" == "Darwin" ];
then

  if [ ! -d "$DESTINATION/.git" ];
  then
    git clone -q ${SRC_URI} ${DESTINATION}
  else
    cur_dir="`pwd`"
    cd "$DESTINATION"
    git pull -q 
    cd "$cur_dir"
  fi

  if [ ! -e "${TARGET_FILE}" ];
  then
    cur_dir="`pwd`"
    cd "$DESTINATION"
    xcodebuild -project AudioSwitcher.xcodeproj
    cd "$cur_dir"
  fi

  if [ ! -e "${BIN_DIR}/AudioSwitcher" ];
  then
    ln -s ${TARGET_FILE} "${BIN_DIR}/AudioSwitcher"
  fi

fi

