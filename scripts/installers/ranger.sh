#!/bin/sh

SRC_URI="https://github.com/ranger/ranger.git"

if [[ "$OSTYPE" == cygwin* ]];
then

  if [ ! -d "$DESTINATION/.git" ];
  then
    git clone -q ${SRC_URI} ${DESTINATION}
  else
    cur_dir="`pwd`"
    cd "$DESTINATION"
    git pull -q 
    cd "$cur_dir"
  fi

  git checkout v1.8.1

  if [ ! -e "$BIN_DIR/ranger" ];
  then
    Sudo ln -s ${DESTINATION}/ranger.py ${BIN_DIR}/ranger
  fi

fi

