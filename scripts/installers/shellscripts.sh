#!/bin/sh

SRC_URI="https://github.com/saironiq/shellscripts.git"

if [ ! -d "$DESTINATION/.git" ];
then
  git clone -q ${SRC_URI} ${DESTINATION}
else
  cur_dir="`pwd`"
  cd "$DESTINATION"
  git pull -q 
  cd "$cur_dir"
fi

# filenames may have spaces
IFS="
"

for f in ${DESTINATION}/*/*.sh;
do
  script_file=${f##*/}

  if [ ! -e "$BIN_DIR/${script_file}" ];
  then
    ln -s ${f} ${BIN_DIR}/${script_file}
  fi

done

