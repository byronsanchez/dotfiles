#!/bin/sh

SRC_URI="https://github.com/stayradiated/dotfiles.git"
APP_DIR="${HOME}/applications"
BUNDLE_DIR="${HOME}/Library/Application Support/SIMBL/Plugins"



if [ "$(uname -s)" == "Darwin" ];
then

  if [ ! -d "$DESTINATION/.git" ];
  then
    git clone -q ${SRC_URI} ${DESTINATION}
  else
    cur_dir="`pwd`"
    cd "$DESTINATION"
    git pull -q
    cd "$cur_dir"
  fi

# filenames may have spaces
  IFS="
"

  for f in ${DESTINATION}/osx/simbl/*.app;
  do
    app_file=${f##*/}

    if [ ! -e "$APP_DIR/${app_file}" ];
    then
      ln -s ${f} ${APP_DIR}/${app_file}
    fi

  done

  for f in ${DESTINATION}/osx/simbl/*.bundle;
  do
    bundle_file=${f##*/}

    if [ ! -e "$BUNDLE_DIR/${bundle_file}" ];
    then
      ln -s "${f}" "${BUNDLE_DIR}/${bundle_file}";
    fi

  done

fi


