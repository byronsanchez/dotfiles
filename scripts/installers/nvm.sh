#!/bin/sh

SRC_URI="git://github.com/creationix/nvm.git"
MY_DESTINATION="${HOME}/.nvm"

if [ ! -d "$MY_DESTINATION/.git" ];
then
  git clone -q ${SRC_URI} ${MY_DESTINATION}
else
  cur_dir="`pwd`"
  cd "$MY_DESTINATION"
  git pull -q 
  cd "$cur_dir"
fi

