#!/bin/sh

SRC_URI="https://github.com/byronsanchez/toggle-osx-shadows.git"
TARGET_FILE="${DESTINATION}/toggle-osx-shadows"

if [ "$(uname -s)" == "Darwin" ];
then

  if [ ! -d "$DESTINATION/.git" ];
  then
    cur_dir="`pwd`"
    git clone -q ${SRC_URI} ${DESTINATION}
    cd ${DESTINATION}
    git checkout -q feature-setmode
    cd "$cur_dir"
  else
    cur_dir="`pwd`"
    cd "$DESTINATION"
    git pull -q 
    cd "$cur_dir"
  fi

  if [ ! -e "${TARGET_FILE}" ];
  then
    cur_dir="`pwd`"
    cd "$DESTINATION"
    make
    cd "$cur_dir"
  fi

  if [ ! -e "${BIN_DIR}/toggle-osx-shadows" ];
  then
    ln -s ${TARGET_FILE} "${BIN_DIR}/toggle-osx-shadows"
  fi

fi

