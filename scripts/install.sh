#!/bin/sh
#
# This installer installs shell scripts!

#DOTFILES_ROOT="$HOME/.dotfiles"
export DOTFILES_ROOT="${HOME}/.dotfiles"
export BIN_DIR="${DOTFILES_ROOT}/bin-links"
export MODULE_DIR="${DOTFILES_ROOT}/modules"
INSTALLER_PATH="${DOTFILES_ROOT}/scripts/installers"

source $DOTFILES_ROOT/scripts/helpers/common.lib.sh

mkdir -p ${MODULE_DIR}
mkdir -p ${BIN_DIR}

for installer in $INSTALLER_PATH/*;
do
  module_file_installer=${installer##*/}
  module_name=${module_file_installer%.sh}
  export DESTINATION="${MODULE_DIR}/${module_name}"

  info "installing ${module_name}..."
  /bin/sh -c "${installer}";
  success "successfully installed ${module_name}..."
done

