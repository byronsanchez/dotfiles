#!/bin/sh

distro=$(cat /etc/redhat-release | cut -f1 -d " ")

if [ "${distro}" == "Gentoo" ];
then

  # Ask for the administrator password upfront
  sudo -v

  # Keep-alive: update existing `sudo` time stamp until this script has finished
  while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

  # Install dropbox daemon
  cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
  # Install dropbox-cli
  wget -O ~/.dotfiles/bin/dropbox.py  https://www.dropbox.com/download?dl=packages/dropbox.py
  chmod +x ~/.dotfiles/bin/dropbox.py

  # TODO: Copy dropbox.py to a bin folder
  # TODO: Make sure dropbox zsh aliases are set (or use shell script 
  # alternatives)

fi

