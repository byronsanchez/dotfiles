#!/usr/bin/env bash

set -e

export DOTFILES="$HOME/.dotfiles"

if ! [ -f $DOTFILES/git/.gitconfig.symlink ]
then

  cat $DOTFILES/git/.gitconfig.symlink.example > $DOTFILES/git/.gitconfig.symlink
  ln -s $DOTFILES/git/.gitconfig.symlink $HOME/.gitconfig

fi

