#!/bin/sh

if [ "$(uname -s)" == "Linux" ];
then
  if ! [ -d "/opt/calibre" ]; then

    # Perform a binary install instead of through the package manager.
    # Linux distro package managers do their own things which often break calibre
    # (eg. private dependency packaging isn't available and causes failures for
    # actual use of some features in calibre, ie. BeautifulSoup for plugins).
    sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.py | sudo python -c "import sys; main=lambda:sys.stderr.write('Download failed\n'); exec(sys.stdin.read()); main()"

  fi
fi

