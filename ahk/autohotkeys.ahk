﻿;Blank Template written by GroggyOtter

;============================== Start Auto-Execution Section ==============================
; Always run as admin
; if not A_IsAdmin
; {
   ; Run *RunAs "%A_ScriptFullPath%"  ; Requires v1.0.92.01+
   ; ExitApp
; }

; Keeps script permanently running
#Persistent

; Determines how fast a script will run (affects CPU utilization).
; The value -1 means the script will run at it's max speed possible.
SetBatchLines, -1

; Avoids checking empty variables to see if they are environment variables.
; Recommended for performance and compatibility with future AutoHotkey releases.
#NoEnv

; Ensures that there is only a single instance of this script running.
#SingleInstance, Force

; Makes a script unconditionally use its own folder as its working directory.
; Ensures a consistent starting directory.
SetWorkingDir %A_ScriptDir%

; sets title matching to search for "containing" instead of "exact"
SetTitleMatchMode, 2

; sets the key send input type.
SendMode, Input

; Sets delay between key strokes and how long a key should be pressed.
; Use this if you need to slow down your key strokes.
; First number is time in ms between key strokes.
; Second number is time in ms that the key is held.
;SetKeyDelay 25, 10

GroupAdd, saveReload, %A_ScriptName%

tts_rate := 5

return

;============================== Save Reload / Quick Stop ==============================
#IfWinActive, ahk_group saveReload

; Use Control+S to save your script and reload it at the same time.
~^s::
	TrayTip, Reloading updated script, %A_ScriptName%
	SetTimer, RemoveTrayTip, 1500
	Sleep, 1750
	Reload
return

; Removes any popped up tray tips.
RemoveTrayTip:
	SetTimer, RemoveTrayTip, Off
	TrayTip
return

; Hard exit that just closes the script
^Esc::
ExitApp


#IfWinActive
;============================== Main Script ==============================

;;
;; functions
;;

TextToSpeech(){
global

If ProcessExist("ptts.exe") {
    ; If the toggle is off (0 or false), do the stuff in here
	commands=
	(join&
	taskkill /F /IM "ptts*"
	taskkill /F /IM "paste*"
	)
	run, %comspec% /c %commands%, , Hide
}
else {
	Send ^c
     ; If the toggle is on (1 or true), do the stuff in here
	commands=
	;; one of these gets stuck, so the kill gets invoked after a 2nd toggle, even after reading has ended. so we kill it here as well.
  ; C:\tools\bin\paste.exe | ptts.exe -r %tts_rate% -voice "Microsoft Zira Desktop - English (United States)"
	(join&
  C:\tools\bin\paste.exe | ptts.exe -r %tts_rate% -voice "Microsoft Eva Mobile - English (United States)"
	taskkill /F /IM "ptts*"
	taskkill /F /IM "paste*"
	)
	run, %comspec% /c %commands%, , Hide
}
}

	ProcessExist(Name){
	Process,Exist,%Name%
	return Errorlevel
	}

	NotifySend(title, message) {
	commands=
	;; one of these gets stuck, so the kill gets invoked after a 2nd toggle, even after reading has ended. so we kill it here as well.
	(join&
  C:\tools\bin\notify-send.exe "[%title%]" "%message%"
  )
	run, %comspec% /c %commands%, , Hide
	}

;;
;; launchers
;;

;; Read the selected text out loud through the sound card
!`::
TextToSpeech()
return

;; To make it cross-compatible with the bindings in Linux
^`::
TextToSpeech()
return

;; NOTE: disabled because it interferes with emacs regex search
;; ^!s::
;; TextToSpeech()
;; return


;;#F20 == Single Click
;;#F19 == Double Click
;;#F18 == Long Click

;; Pen button click reads selected text
<#F20::
TextToSpeech()
return

;; Right-button click reads selected text
; ~RButton::
;; Send ^c
;return

;; Increase tts speech rate
^!k::
tts_rate += 1
if (tts_rate >= 10) {
  tts_rate = 10
}
NotifySend("tts", "speech rate: " . tts_rate)
return

;; Decrease tts speech rate
^!j::
tts_rate -= 1
if (tts_rate <= -10) {
  tts_rate = -10
}
NotifySend("tts", "speech rate: " . tts_rate)
return

;;
;; media
;;

;;  Switch to prev song
#!Left::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe prev-track
return

;; Switch to next song
#!Right::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe next-track
return

;; Toggle between play and paused states
#!p::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe play-pause
return

;; Increase volume
#!Up::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe volume-up
return

;; Decrease volume
#!Down::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe volume-down
return

;;  Toggle random mode
#!z::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe toggle-shuffle
return

;;  Toggle repeat mode
#!r::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe cycle-repeat
return

;;  Toggle single mode
#!y::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe cycle-repeat
return

;;  Display current song info in a notification pop-up
#!c::
IfWinExist, ahk_class iTunes
Run call-hotkey.exe show-current
return

#!n::
IfWinNotExist, ahk_class iTunes
{
Run %ProgramFiles%\iTunes\iTunes.exe  ;launch program
return
}

IfWinExist, ahk_class iTunes ; toggle minimize/restore
{
IfWinNotActive ; restores window
WinActivate
Else
WinMinimize ; minimizes windows
return
}

;;#F20 == Single Click
;;#F19 == Double Click
;;#F18 == Long Click

; Toggle between OneNote and Drawboard
; Framework-cframe is OneNote
; #IfWinNotExist, ahk_class Framework::CFrame
   ; ; We only make sure OneNote is run once or multiple instances will launch
   ; ; drawboard doesn't have this issue, so this logic where it is run more than 
   ; ; once is okay
   ; #F20::Run, OneNote

; #IfWinNotExist, Drawboard PDF
   ; #F19::Run drawboardpdf:

; #IfWinExist, ahk_class Framework::CFrame
; {

; #IfWinNotActive, ahk_class Framework::CFrame
   ; ; We only make sure OneNote is run once or multiple instances will launch
   ; ; drawboard doesn't have this issue, so this logic where it is run more than 
   ; ; once is okay
   ; #F20::WinActivate, OneNote

; #IfWinActive, ahk_class Framework::CFrame
    ; #F18::F11                      ; minimal window
    ; #F20::Run drawboardpdf:              ;Minimize OneNote Window
; }

; #IfWinExist, Drawboard PDF
; {
; #IfWinNotActive, Drawboard PDF
   ; #F19::Run drawboardpdf:

; #IfWinActive, Drawboard PDF
    ; #F18::Send {Ctrl down}`t{Ctrl up}   ; Tab between open books
    ; #F20::WinActivate, OneNote

; }

;============================== Program 1 ==============================
; Evertything between here and the next #IfWinActive will ONLY work in someProgram.exe
; This is called being "context sensitive"
; #IfWinActive, ahk_exe someProgram.exe



; #IfWinActive


;============================== Drawboard ==============================
; Evertything between here and the next #IfWinActive will ONLY work in someProgram.exe
; This is called being "context sensitive"
; #IfWinActive, Drawboard
;
;
; #IfWinActive

;============================== ini Section ==============================
; Do not remove /* or */ from this section. Only modify if you're
; storing values to this file that need to be permanantly saved.
/*
[iniSection]
Key=Value
*/
;============================== GroggyOtter ==============================
;============================== End Script ==============================



