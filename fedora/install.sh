#!/bin/sh

distro=$(cat /etc/redhat-release | cut -f1 -d " ")

if [ "${distro}" == "Fedora" ];
then

  # Ask for the administrator password upfront
  sudo -v

  # Keep-alive: update existing `sudo` time stamp until this script has finished
  while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

  # TODO: extract /etc/thinkfan.conf
  # TODO: Configure random wallpaper for Gnome 3 Wayland

  ###############################################################################
  # Workstation Packages                                                        #
  ###############################################################################

  sudo dnf install -y gnome-tweak-tool
  sudo dnf install -y vim
  sudo dnf install -y ranger
  sudo dnf install -y htop
  sudo dnf install -y iotop
  sudo dnf install -y powertop
  sudo dnf install -y tree
  sudo dnf install -y at
  sudo dnf install -y screen
  sudo dnf install -y tmux
  # used by zsh for rsync's autocompletes
  sudo dnf install -y yp-tools
  sudo dnf install -y rpmconf
  # hotkeys
  sudo dnf install -y sxhkd
  sudo dnf install -y the_silver_searcher
  sudo dnf install -y isync
  # used by my local isync python script, via an import
  sudo dnf install -y offlineimap
  # dependency required by the isync offlineimap import
  sudo dnf install -y python2-six
  sudo dnf install -y git
  sudo dnf install -y fossil
  # TODO: Configure these services with systemctl
  sudo dnf install -y rkhunter
  sudo dnf install -y chkrootkit
  sudo dnf install -y calibre

  # spotify

  # didn't have spotify in the repo ala docker stable when I tried it
  # sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
  # https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
  # sudo dnf install -y spotify

  # too many selinux violations; can't use; like over 50, spotify kept crashing, 
  # etc.
  # sudo dnf install snapd
  # sudo ln -s /var/lib/snapd/snap /snap
  # snap install spotify

  sudo dnf config-manager --add-repo=http://negativo17.org/repos/fedora-spotify.repo
  sudo dnf install -y spotify

  ###############################################################################
  # Emacs Packages                                                              #
  ###############################################################################

  # Used by emacs to import ssh-agent and gpg-agent environment so that magit 
  # and eshell can leverage it, read yubikey auths, etc.
  #
  # Okay ended up not using it, but wanna keep it to remember I can use it in 
  # the future if I really need to
  sudo dnf install -y emacs
  sudo dnf install -y keychain
  sudo dnf install -y aspell
  sudo dnf install -y aspell-en

  ###############################################################################
  # Node Packages                                                               #
  ###############################################################################

  curl --silent --location https://rpm.nodesource.com/setup_10.x | sudo bash -
  sudo yum -y install nodejs

  ###############################################################################
  # Docker Packages                                                             #
  ###############################################################################

  # install dnf-plugins core package which provides commands to manage your dnf
  # repos from cli
  sudo dnf -y install dnf-plugins-core
  # Set up the stable repository (even in edge or test, you'll always need a
  # stable)
  sudo dnf config-manager \
       --add-repo \
       https://download.docker.com/linux/fedora/docker-ce.repo
  # Uncomment if you ever want the edge and test repositories enabled; they are
  # disabled by default; you can subscribe to multiple repositories and get the
  # latest one from the set of all 3. Eg. Stable releases aren't published in
  # Edge, so by subscribing to Stable /and/ Edge, you'll get the edge releases
  # /and/ the stable releases and install the latest of the 2. Same with test. I
  # think test is the bleeding edge, while Edge is the preview/beta. Given that
  # when I am setting this up (2018-05-07), the stable channel has no package
  # available for Fedora 28, I'm concluding that I should go bleeding edge for a
  # reliable developer workstation, so I'm enabling test. On production systems,
  # I'd go for something stable that doesn't update features every month, only
  # security fixes.
  #
  # This guy also likes the bleeding edge approach so I'm good:
  #
  # https://github.com/docker/for-linux/issues/295
  #
  # Oh, and edge and test will get stable releases now:
  #
  # https://docs.docker.com/install/linux/docker-ce/fedora/#install-docker-ce
  #
  # Yup, that link confirms that with multiple repos, most recent wins.
  sudo dnf config-manager --set-enabled docker-ce-edge
  sudo dnf config-manager --set-enabled docker-ce-test
  # Install the /latest version/ of Docker CE
  sudo dnf install -y docker-ce
  # NOTE: Update the version number in the url path by using the latest release 
  # from:
  #
  # https://github.com/docker/compose/releases
  sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose

  ###############################################################################
  # TODO: RPMFusion                                                        #
  ###############################################################################

  ###############################################################################
  # Config Files                                                                #
  ###############################################################################

  ###############################################################################
  # SSH                                                                         #
  ###############################################################################

  sudo dnf install -y openssh
  sudo dnf install -y openssh-server
  sudo cp ${HOME}/.dotfiles/fedora/files/etc/ssh/sshd_config /etc/ssh/sshd_config
  sudo semanage port -a -t ssh_port_t -p tcp 1224
  sudo systemctl enable sshd;

  ###############################################################################
  # rsnapshot                                                                   
  ###############################################################################

  ##
  ## rsnapshot host
  ##

  # required by rsnapshot, but not listed in rpm properly
  sudo dnf install -y perl-Lchown

  sudo cp ~/Dropbox/hackBytes/Projects/byronsanchez/backups/backitup.sh /usr/local/bin/backitup
  sudo chmod +x /usr/local/bin/backitup
  sudo cp ~/Dropbox/hackBytes/Projects/byronsanchez/backups/cryptshot.sh /usr/local/bin/cryptshot
  sudo chmod +x /usr/local/bin/cryptshot
  sudo cp ~/Dropbox/hackBytes/Projects/byronsanchez/backups/tarsnapper.py /usr/local/bin/tarsnapper.py
  sudo chmod +x /usr/local/bin/tarsnapper.py

  # TODO: Move keyfiles to a private directory

  sudo dnf install -y rsnapshot
  sudo cp ${HOME}/.dotfiles/fedora/files/etc/rsnapshot.conf /etc/rsnapshot.conf;
  sudo cp ${HOME}/.dotfiles/fedora/files/etc/rsnapshot.exclude /etc/rsnapshot.exclude;
  sudo cp ${HOME}/.dotfiles/fedora/files/etc/cron.d/rsnapshot /etc/cron.d/rsnapshot;

  sudo mkdir /root/.ssh
  # MANUAL: Copy ${PRIVATE}/rbackup/.ssh/rbackup_rsa to /root/.ssh
  # MANUAL: Copy ${PRIVATE}/rbackup/.ssh/rbackup_rsa.pub to /root/.ssh
  # MANUAL: Register all client workstations as known hosts via ssh rbackup@target.node.io
  # MANUAL: Copy ${PRIVATE}/root/backup_key to /root/backup_key
  sudo chown -R root:root /root/.ssh 
  sudo chown root:root /root/backup_key
  sudo chmod -700 /root/.ssh
  sudo chmod -600 /root/.ssh/id_rsa
  sudo chmod -600 /root/.ssh/id_rsa.pub
  sudo chmod -600 /root/.ssh/known_hosts

  # to store process cache data
  sudo mkdir -p /var/lib/nitelite/backup/

  ##
  ## rsnapshot client- not used yet, so incomplete
  ##

  # TODO: Finish implementing client
  sudo mkdir /home/rbackup/.ssh
  # MANUAL: Copy ${PRIVATE}/rbackup/.ssh/rbackup_rsa to /home/rbackup/.ssh
  # MANUAL: Copy ${PRIVATE}/rbackup/.ssh/rbackup_rsa.pub to /home/rbackup/.ssh
  # MANUAL: Copy ${PRIVATE}/rbackup/.ssh/authorized_keys to /home/rbackup/.ssh
  sudo cp ${HOME}/.dotfiles/fedora/files/usr/bin/rsync_wrapper.sh /usr/bin/rsync_wrapper.sh
  sudo chmod +x /usr/bin/rsync_wrapper.sh

  ###############################################################################
  # Text-to-speech (TTS)                                                        #
  ###############################################################################

  sudo dnf install -y wine
  curl -C - -L -o /tmp/winetricks  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
  chmod +x /tmp/winetricks
  sudo mv /tmp/winetricks /usr/local/bin
  sudo chown root:root /usr/local/bin/winetricks
  winetricks msxml3
  wine msiexec /i ~/Dropbox/hackBytes/Applications/TTS/SpeechPlatformRuntime.msi
  wine msiexec /i ~/Dropbox/hackBytes/Applications/TTS/MSSpeech_TTS_en-US_ZiraPro.msi
  wine msiexec /i ~/Dropbox/hackBytes/Applications/TTS/MSSpeech_TTS_en-US_Helen.msi
  wine msiexec /i ~/Dropbox/hackBytes/Applications/TTS/MSSpeech_TTS_en-GB_Hazel.msi

  # yes, xclip is compatible with Wayland - needed for piping text selections to
  # wine with tts keybindings
  sudo dnf install -y xclip

  ###############################################################################
  # YubiKey                                                                     #
  ###############################################################################

  # yubikey tools
  sudo dnf install -y yubikey-manager
  sudo dnf install -y yubikey-personalization-gui
  sudo dnf install -y yubico-piv-tool
  # yubikey gpg smartcard support
  sudo dnf install -y gnupg-pkcs11-scd
  # generic smartcard support
  sudo dnf install -y opensc
  sudo dnf install -y fuse-encfs

  ###############################################################################
  # Zsh                                                                         #
  ###############################################################################

  # zsh
  sudo dnf install -y zsh
  chsh -s /bin/zsh

  ###############################################################################
  # Dropbox                                                                     #
  ###############################################################################

  # deps+dropbox
  sudo dnf install -y python-gpgme
  sudo dnf install -y https://www.dropbox.com/download?dl=packages/fedora/nautilus-dropbox-2015.10.28-1.fedora.x86_64.rpm
  sudo cp ${HOME}/.dotfiles/fedora/files/etc/sysctl.conf /etc/sysctl.conf;
  sudo sysctl -p

  ###############################################################################
  # Google Chrome                                                               #
  ###############################################################################

  # Google Chrome
  #
  # gpgverification+chrome
  # w/o verification, chrome install errors out
  lp_target=/tmp/laptop
  mkdir -p "${lp_target}"
  curl -C - -L -o "${lp_target}/linux_signing_key.pub" https://dl.google.com/linux/linux_signing_key.pub
  sudo rpm --import "${lp_target}"
  sudo dnf install -y https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
  rm ${lp_target}/linux_signing_key.pub
  rmdir ${lp_target}

  ###############################################################################
  # Laptop Fan Management                                                       #
  ###############################################################################

  # LAPTOP SETUP FOR THINKPAD T420:
  #
  # Links:
  #
  # http://www.posix.in-egypt.net/content/t420-fan-management-fedora
  # https://forums.lenovo.com/t5/Linux-Discussion/My-T420-and-Mint-12-Experience/td-p/684615
  #
  # laptop tools
  # how to use: 
  # https://askubuntu.com/questions/15832/how-do-i-get-the-cpu-temperature?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
  sudo dnf install -y lm_sensors
  sudo dnf install -y lm_sensors-sensord
  # only if you use a hdd not ssd
  sudo dnf install -y hdapsd
  sudo dnf install -y hddtemp
  # fan
  sudo dnf install -y thinkfan
  sudo sensors-detect
  # NOTE: This will keep appending the hwmon values, so manually edit if you run 
  # the script more than once, and remove dupes
  sudo sh -c "find /sys/devices -type f -name 'temp*_input' | xargs -I {} echo "hwmon {}" >> /etc/thinkpad.conf"
  sudo systemctl enable thinkfan

  ###############################################################################
  # Laptop Power Management                                                     #
  ###############################################################################

  ##
  ## power management - tlp
  ##

  #sudo dnf install -y tlp tlp-rdw kernel-devel akmods kmodtool
  #sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
  #sudo dnf install -y http://repo.linrunner.de/fedora/tlp/repos/releases/tlp-release-1.0-0.noarch.rpm
  #sudo dnf install -y http://repo.linrunner.de/fedora/tlp/repos/releases/tlp-release.fc$(rpm -E %fedora).noarch.rpm
  #sudo dnf install -y akmod-tp_smapi akmod-acpi_call --enablerepo tlp-updates-testing --nogpgcheck

  ##
  ## power management - tuned
  ##

  sudo dnf install -y tuned-utils
  sudo powertop2tuned -n -e laptop

  ###############################################################################
  # Laptop CPU Frequency Scaling                                             #
  ###############################################################################

  sudo dnf install -y thermald
  sudo cp ${HOME}/.dotfiles/fedora/files/etc/thermald/thermal-conf.xml /etc/thermald/thermal-conf.xml
  sudo systemctl enable thermald
  sudo systemctl start thermald
  #sudo dnf install -y cpufreq
  sudo dnf install -y stress-ng

  ###############################################################################
  # JetBrains IDEs                                                              #
  ###############################################################################

  sudo tar xfz ~/Dropbox/hackBytes/Applications/JetBrains/jetbrains-toolbox-1.8.3678.tar.gz -C /opt/
  # Just use the toolbox, like Adobe CC
  # sudo tar xfz ~/Dropbox/hackBytes/Applications/JetBrains/WebStorm-2018.1.2.tar.gz -C /opt/
  # sudo tar xfz ~/Dropbox/hackBytes/Applications/JetBrains/RubyMine-2018.1.1.tar.gz -C /opt/

fi

