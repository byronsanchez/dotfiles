preexec () {
  if [ -n "$STY" ];
  then
    # Remove cwd so it doesn't get added to directory paths in PS1 because of 
    # zsh's AUTO_NAME_DIRS option
    #cwd=`pwd`
    nwd=@${cwd/\/home\/byronsanchez/\~}
    echo -ne "\ek${1%% *}${nwd%% *}\e\\"
  fi
}

# This check is required for eshells to be able to ssh into machine's with users 
# using zsh!
if [[ "$TERM" == "dumb" ]]
then
  unsetopt zle
  unsetopt prompt_cr
  unsetopt prompt_subst
  unfunction precmd
  unfunction preexec
  PS1='$ '
fi

# shortcut to this dotfiles path is $DOTFILES
export DOTFILES="$HOME/.dotfiles"

# shortcut to zprezto
export ZSH="$HOME/.zprezto"

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]];
then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# all of our zsh files
typeset -U config_files
config_files=($DOTFILES/**/*.zsh)

# load the path files
for file in ${(M)config_files:#*/path.zsh}
do
  source $file
done

 # load everything but the path and completion files
for file in ${${config_files:#*/path.zsh}:#*/completion.zsh}
do
  source $file
done

# initialize autocomplete here, otherwise functions won't be loaded
autoload -U compinit
compinit

# load every completion after autocomplete loads
for file in ${(M)config_files:#*/completion.zsh}
do
  source $file
done

unset config_files

# load rvm into shell as a function
[ -e "$HOME/.rvm/scripts/rvm" ] && source "$HOME/.rvm/scripts/rvm"
# rvm + screen seems to change umask to 002. switch back to system default
#umask 022

# vi-mode fixes
#bindkey '^r' history-incremental-search-backward    # [Ctrl-r] - Search 
#backward incrementally for a specified string. The string may begin with ^ to 
#anchor the search to the beginning of the line.
#bindkey '^?' backward-delete-char
#bindkey '^a' beginning-of-line
#bindkey '^e' end-of-line


# Bind alt-f and alt-b for word moving.
#bindkey '^[f' forward-word
#bindkey '^[b' backward-word

export NVM_DIR="/home/byronsanchez/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm


# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Hack for Intellij terminals - Auto-open in project root directory if an 
# intellij terminal is launched under a project
[[ "$OLDPWD" == '/cygdrive/c/WINDOWS/system32' ]] || cd -

