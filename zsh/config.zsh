# custom zsh configuration options

fpath=($DOTFILES/functions $fpath)

autoload -U $DOTFILES/functions/*(:t)

COMMAND_MODE=unix2003

setopt NO_BG_NICE # don't nice background tasks
setopt NO_HUP
setopt NO_LIST_BEEP
setopt LOCAL_OPTIONS # allow functions to have local options
setopt LOCAL_TRAPS # allow functions to have local traps
setopt PROMPT_SUBST
setopt CORRECT
setopt COMPLETE_IN_WORD
setopt IGNORE_EOF

# don't expand aliases _before_ completion has finished
#   like: git comm-[tab]
setopt complete_aliases

# required for rvm
setopt nullglob

unsetopt promptcr
# Disable an option set by the directory module which detects variables
# containing paths and replacing the path in the prompt PS1 with that variable
# (results in things like the cwd bug, RVM_CURRENT_PROJECT), etc.
unsetopt AUTO_NAME_DIRS       # Disable Auto add variable-stored paths to ~ list.

zle -N newtab

# Make sure killing words emacs-style isn't too liberal (stop at
# nonalphanumerics!)
autoload -U select-word-style
select-word-style bash

