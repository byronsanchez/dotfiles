#
# Aliases
#

source $HOME/.dotfiles/scripts/helpers/common.lib.sh

# grc overides for ls
#   Made possible through contributions from generous benefactors like
#   `brew install coreutils`
if $(gls &>/dev/null)
then
  alias ls="gls -F --color"
  alias l="gls -lAh --color"
  alias ll="gls -l --color"
  alias la="gls -A --color"
fi

alias grep="grep --color=auto"

alias today="encfsmount && n log/`date +%Y`/`date +%Y-%m-%d`-log-entry.md"

alias reload!='. ~/.zshrc'

alias bmount="sudo mount -o uid=byronsanchez,gid=byronsanchez"
alias bumount="sudo umount"

# Global aliases
alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'

# PS
alias psa="ps aux"
alias psg="ps aux | grep "

# Show human friendly numbers and colors
alias df='df -h'
alias ll='ls -alGh'
alias ls='ls -GhF --color'
alias du='du -h -d 2'

# Common shell functions
alias cl='clear'

alias ka9='killall -9'
alias k9='kill -9'

# Ruby alises
alias brake='bundle exec rake'
alias bruby='bundle exec ruby'

# Override rm -i alias which makes rm prompt for every action
alias rm='nocorrect rm'

# Don't try to glob with zsh so you can do
# stuff like ga *foo* and correctly have
# git add the right stuff
alias git='noglob git'
alias rake='noglob rake'

# Use zmv, which is amazing
autoload -U zmv
alias zmv="noglob zmv -W"

alias fs="fossil"

alias skype="skype_wrapper"

alias .n="encfsmount"
alias ,n="encfsdismount"
alias .nb="encfsmount && cd $NOTEDIR && fossil server"
alias ,nb="killall -9 fossil && encfsdismount"
alias ledger="encfsmount && /usr/bin/ledger"

alias v="killall cava 2>/dev/null; sleep 0.1; cava -s 200% -b 200 -c green -m normal"

alias x="startx ~/.xsession-bspwm"

alias chrome="google-chrome"

# We can use -n here since this alias is meant to be manually invoked
if stringContain "bsanchez" "$(hostname)"
then
  alias e="emacsclientw -na \"\" -c -F \"((width . 188) (height . 60))\""
else
  alias e="emacsclient -na \"\" -c"
fi

# Forgot the original reason I put this here, but might also be making gpg 
# decrypting work on cygwin with vim-gnupg. Because vim-gnupg specifies the 
# executable as gpg2 to handle the Fedora case, but cygwin might not have gpg2 
# (since Windows uses native gnupg not cygwin gnupg). So if anything breaks, 
# look into these parts.
if [[ "$OSTYPE" == cygwin* ]];
then
  alias gpg2="gpg"
fi

# On linux, we can use Windows TTS via Wine
if [[ "$OSTYPE" == linux* ]];
then
  alias wtts="wine ~/Dropbox/hackBytes/Applications/Bins/balcon/balcon.exe"
fi

# Gentoo used these
#alias dropbox="~/.dotfiles/bin/dropbox.py"
#alias dropboxd="~/.dropbox/dropboxd"

#alias google-chrome-stable="google-chrome-beta"

