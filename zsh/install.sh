#!/usr/bin/env bash

set -e

if [ -n "$ZSH_VERSION"]
then
  current_shell="zsh"
fi

if [ "$(uname -s)" == "Linux" ];
then
  if [ "$current_shell" != "zsh" ]
  then
    chsh -s /bin/zsh
  fi
fi

if [ ! -d "$HOME/.zprezto/.git" ];
then
  git clone --recursive git://git.hackbytes.io/byronsanchez/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
else
  cur_dir="`pwd`"
  cd "$HOME/.zprezto"
  git pull -q && git submodule update --init --recursive
  cd "$cur_dir"
fi

