#!/bin/sh
#
# Used with xbindkeys to speak the selected text.
#
# TODO: Still doesn't immediately pause when the script is called while a
# selection is being read.

source $HOME/.dotfiles/scripts/helpers/common.lib.sh

###############
# CONFIGURATION

# Set defaults
c_notifier=""
config_file=""
c_rate=
c_lang=

quiet=0

############################
# Argument Parsing Functions

function show_help ()
{
cat <<-EOM

$0 [OPTION] command

options:

    -n --notifier=NAME          notification command to execute; if none, no notification will be sent
    -r -rate=RATE               rate of voice; default is 100
    -l -language=LANGUAGE       rate of voice; default is 100
    -c --config-file=NAME       config file used to set the above values

commands:

    speak               speaks
    rate-increase       increase rate of speech
    rate-decrease       decrease rate of speech

other:

    -q --quiet               do not log messages to STDOUT
    -h --help                display this message

EOM
    exit 1
}

function get_options () {
    argv=()
    while [ $# -gt 0 ]
    do
        opt=$1
        shift
        case ${opt} in
            -n|--notifier)
                c_notifier=$1
                shift
                ;;
            --notifier=*)
                c_notifier=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -r|--rate)
                c_rate=$1
                shift
                ;;
            --rate=*)
                c_rate=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -l|--lang)
                c_lang=$1
                shift
                ;;
            --lang=*)
                c_lang=$(echo ${opt} | cut -d'=' -f 2);
                ;;

            -c|--config-file)
                config_file=$1
                shift
                ;;
            --config-file=*)
                config_file=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -q|--quiet)
                quiet=1
                ;;

            -h|--help)
                show_help
                ;;
            *)
                if [ "${opt:0:1}" = "-" ]; then
                    fail "${opt}: unknown option."
                else
                  # The action is the first argument passed with no associated
                  # option. If more than one is passed, the most recent will
                  # override and will be the action that is executed.
                  action=${opt}
                fi
                argv+=(${opt})
                ;;
        esac
    done
}

function notifier() {
  f_notifier=$1
  f_title=$2
  f_message=$3

  # Send the notification to the appropriate program
  if [ -n "$f_notifier" ] && [ -n "$f_message" ];
  then
    if [ $f_notifier = "terminal-notifier" ];
    then
      terminal-notifier -message "$f_message" -title "$f_title"
    elif [ $f_notifier = "ratpoison" ];
    then
      ratpoison -d :0 -c "echo [$f_title] $f_message"
    elif [ $notifier = "dunstify" ];
    then
      dunstify -r 1 "[$f_title]" "$f_message"
    elif [ $notifier = "notify-send" ];
    then
      notify-send "[$f_title]" "$f_message"
    else
      # treat the notifer as a command and pass it the title and message as a
      # string
      $f_notifier "[$f_title] $f_message"
    fi
  fi
}

############
# VALIDATION

unset message

# Parse options if they were passed; will override configs
get_options $*

if [ ! -n "${config_file}" ];
then
  config_file="${HOME}/.dotfiles-settings/config"
fi

if [ -e "${config_file}" ];
then
  source "${config_file}"
fi

# command-line defined opts overrides the .nldeploy file settings
if [ -n "${c_rate}" ];
then
  rate="${c_rate}"
fi

# command-line defined opts overrides the .nldeploy file settings
if [ -n "${c_lang}" ];
then
  lang="${c_lang}"
fi

# command-line defined opts overrides the .nldeploy file settings
if [ -n "${c_notifier}" ];
then
  notifier="${c_notifier}"
fi

if [ ! -n "$action" ];
then
  fail "Please provide a valid command to execute"
fi

#############
# MAIN SCRIPT

title="tts"

case "$action" in

rate-increase)
  #new_rate=$((rate+25))
  new_rate=$((rate+1))

  echo "changing rate from $rate to $new_rate "
  echo $config_file

  key="rate"
  value="${new_rate}"
  sed -i -r "s/($key *= *\").*/\1$value\"/" ${config_file}
  message="speech rate: ${new_rate}"
  ;;

rate-decrease)

  #new_rate=$((rate-25))
  new_rate=$((rate-1))

  echo "changing rate from $rate to $new_rate "

  key="rate"
  value="${new_rate}"
  sed -i -r "s/($key *= *\").*/\1$value\"/" ${config_file}
  message="speech rate: ${new_rate}"
  ;;

speak)

  #isEspeakSpeaking=$(ps aux | grep picospeaker | grep -v grep | grep -v tts);
  isEspeakSpeaking=$(ps aux | grep wine | grep -v grep | grep -v tts);

  if [ ! -n "$isEspeakSpeaking" ];
  then
    export WINEARCH=win32
    export WINEPREFIX=~/.wine32

    #xclip -o | picospeaker -l ${lang} -r ${rate} -p -2;
    xclip -selection primary -o | wine ~/Dropbox/hackBytes/Applications/Bins/balcon/balcon.exe -i -s "${rate}" -n "${lang}"
    # gracefully kill the wine process once balcon is done speaking
    wineserver -k 9
  else
    #killall play;
    wineserver -k 9
    #${HOME}/.dotfiles/bin/killwine
  fi

  ;;
*)
  fail "${action}: unknown command."
  ;;
esac

if [ -n "$notifier" ];
then
  notifier "${notifier}" "${title}" "${message}"
fi


