#!/bin/sh
#
# This script is to be called manually after popfile gets a chance to classify
# the emails

# TODO: Make sure only a specific mailbox is targetted at a time (or hardcode
# the mailbox)

TAGS="
akl
bmx
byronsanchez
byronsanchez-career
byronsanchez-dating
byronsanchez-dream
byronsanchez-finance
byronsanchez-firefighting
byronsanchez-health
byronsanchez-mooc
byronsanchez-personal
byronsanchez-politics
byronsanchez-sales
byronsanchez-social
byronsanchez-software
byronsanchez-tsm
ccp
chompix
globide
hackbytes
nitelite
psu
psu-alumni
psu-ballroom-dance-club
psu-classes
psu-clown-nose-club
psu-homanprovements
psu-outing-club
psu-soaring-club
psu-south-food-district
psu-student-red-cross-club
psu-the-ngcredibles
spam
the-project
tpk
"

function set_tag_for_filter () {
    TARGET_TAG="$1"
    TARGET_FILTER="$2"
    TAG_LIST="$3"

    echo "Setting tag for filter:"
    echo "  TARGET_TAG: $1"
    echo "  TARGET_FILTER: $2"

    # Clear all bucket tags from the specified filter. This way, the filter results
    # will only be tagged once.

    for tag in $TAG_LIST; do
        if [ -n "${tag}" ];
        then

          if [ "${tag}" == "${TARGET_TAG}" ];
          then
            continue;
          fi

          # echo "    -- Iterating for tag ${tag}: "
          # echo "    -- Executing: notmuch tag" "-${tag}" "${TARGET_FILTER}"
          notmuch tag -${tag} ${TARGET_FILTER}
        fi
    done

    # Add the specified tag to the result set of the specified filter
    # echo "  -- ---------------------------------------------------------"
    echo "  -- Executing: notmuch tag" "+${TARGET_TAG}" -- "${TARGET_FILTER} NOT tag:${TARGET_TAG}"
    # echo "  -- ---------------------------------------------------------"
    notmuch tag +${TARGET_TAG} -- ${TARGET_FILTER} NOT tag:${TARGET_TAG}
}

# byronsanchez-tsm
set_tag_for_filter "byronsanchez-tsm" "from:71anddone@gmail.com" "${TAGS}"
set_tag_for_filter "byronsanchez-tsm" "from:info@the21convention.com" "${TAGS}"
set_tag_for_filter "byronsanchez-tsm" "from:alex@alexsocial.com" "${TAGS}"
set_tag_for_filter "byronsanchez-tsm" "from:chris@goodlookingloser.com" "${TAGS}"
set_tag_for_filter "byronsanchez-tsm" "from:rsdnation.com" "${TAGS}"

# byronsanchez-mooc
set_tag_for_filter "byronsanchez-mooc" "from:coursera.org" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:hackdesign.org" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:mongodb.com" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:10gen.com" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:hpi.uni-potsdam.de" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:app21.ilearn.marist.edu" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:marist.edu" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:stanford.edu" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:academia.edu" "${TAGS}"
set_tag_for_filter "byronsanchez-mooc" "from:udacity.com" "${TAGS}"

# akl
set_tag_for_filter "akl" "to:L-THON-ALUMNI@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "akl" "from:dmaig.org" "${TAGS}"
set_tag_for_filter "akl" "to:L-AKLTAU@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "akl" "to:L-THON-NEWS@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "akl" "from:omegafi.com" "${TAGS}"
set_tag_for_filter "akl" "from:chapterboard.com" "${TAGS}"
set_tag_for_filter "akl" "from:communications@thon.org" "${TAGS}"

# ccp
set_tag_for_filter "ccp" "from:ccp.edu" "${TAGS}"
set_tag_for_filter "ccp" "from:notifications@instructure.com" "${TAGS}"
set_tag_for_filter "ccp" "from:no-reply@orgsync.com" "${TAGS}"
set_tag_for_filter "ccp" "from:sendwordnow.com" "${TAGS}"

# politics
set_tag_for_filter "ccp" "from:donaldjtrump.com" "${TAGS}"
set_tag_for_filter "ccp" "to:bsanche9@ccp.edu" "${TAGS}"

# bdc
set_tag_for_filter "psu-ballroom-dance-club" "to:BDC@lists.psu.edu" "${TAGS}"
# cnc
set_tag_for_filter "psu-clown-nose-club" "to:L-CLOWN-NOSE-CLUB@lists.psu.edu" "${TAGS}"
# ingcredibles
set_tag_for_filter "psu-the-ngcredibles" "to:ingcredibles@googlegroups.com" "${TAGS}"
# homanprovements
set_tag_for_filter "psu-homanprovements" "to:homanprovements2012@googlegroups.com" "${TAGS}"
# penn state soaring club
set_tag_for_filter "psu-soaring-club" "to:L-PSSC@lists.psu.edu" "${TAGS}"
# penn state outing club
set_tag_for_filter "psu-outing-club" "to:L-PSOC@lists.psu.edu" "${TAGS}"
# student red cross club
set_tag_for_filter "psu-student-red-cross-club" "to:L-RED-CROSS@lists.psu.edu" "${TAGS}"
# south food district
set_tag_for_filter "psu-south-food-district" "from:rediferscheduler@yahoo.com" "${TAGS}"
set_tag_for_filter "psu-south-food-district" "from:whentowork.com" "${TAGS}"

# career
set_tag_for_filter "byronsanchez-career" "from:Dominion-Enterprises@jobs.net" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:indeed.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:monster.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:simplyhired.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:jobs-listings@linkedin.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:job-listings@linkedin.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:inmail-hit-reply@linkedin.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:hit-reply@linkedin.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:careerbuilder.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:higheredjobs.com" "${TAGS}"
set_tag_for_filter "byronsanchez-career" "from:barefootstudent.com" "${TAGS}"

# byronsanchez-finance
#notmuch tag +TODO from:support@namecheap.com
#notmuch tag +TODO from:namecheap.com
#notmuch tag +TODO from:apple.com
set_tag_for_filter "byronsanchez-finance" "from:mygreatlakes.org" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:paypal.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:info@borrowerservices.mygreatlakes.org" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:AmericanExpress@welcome.aexp.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:noreply@mailer.citizensone.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:no_reply@email.apple.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:amazon.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:ebay.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:att-mail.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:googleplay-noreply@google.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:admob-noreply@google.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:steampowered.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:adp.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:6pm.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:vision-direct.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:eueyewear.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:gouletpens.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:irs.gov" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:localupsolutions.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:newegg.com" "${TAGS}"
set_tag_for_filter "byronsanchez-finance" "from:billing@linode.com" "${TAGS}"

set_tag_for_filter "byronsanchez-personal" "from:googlealerts-noreply@google.com" "${TAGS}"

set_tag_for_filter "bmx" "from:behaviormatrix.com" "${TAGS}"
set_tag_for_filter "bmx" "from:BehaviorMatrix" "${TAGS}"

set_tag_for_filter "hackbytes" "from:travis-ci.org" "${TAGS}"
set_tag_for_filter "hackbytes" "from:docker.io" "${TAGS}"
set_tag_for_filter "hackbytes" "from:github.com" "${TAGS}"
set_tag_for_filter "hackbytes" "from:bitbucket.org" "${TAGS}"
set_tag_for_filter "hackbytes" "from:atlassian.com" "${TAGS}"
set_tag_for_filter "hackbytes" "from:zohocorp.com" "${TAGS}"
set_tag_for_filter "hackbytes" "from:zoho.com" "${TAGS}"

# nitelite
set_tag_for_filter "nitelite" "from:icloud.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:linode.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:accounts.google.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:appleid@id.apple.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:cloudflare.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:frantech.ca" "${TAGS}"
set_tag_for_filter "nitelite" "from:vpsboard.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:rescuetime.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:tarsnap.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:namecheap.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:feedly.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:dropbox.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:archive.org" "${TAGS}"
set_tag_for_filter "nitelite" "from:startcom.org" "${TAGS}"
set_tag_for_filter "nitelite" "from:noip.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:freenode.net" "${TAGS}"
set_tag_for_filter "nitelite" "from:quakenet.org" "${TAGS}"
set_tag_for_filter "nitelite" "from:noreply-analytics@google.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:\"Google Takeout\"" "${TAGS}"
set_tag_for_filter "nitelite" "from:salesforce.com" "${TAGS}"
set_tag_for_filter "nitelite" "from:hootsuite.com" "${TAGS}"

# psu-classes
set_tag_for_filter "psu-classes" "to:L-ART2@lists.psu.edu" "${TAGS}"
set_tag_for_filter "psu-classes" "from:REGISTRAR@psu.edu" "${TAGS}"
set_tag_for_filter "psu-classes" "from:StudyAbroad.com" "${TAGS}"
set_tag_for_filter "psu-classes" "to:L-RPTM-STUDY-ABROAD@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "psu-classes" "from:la.psu.edu" "${TAGS}"
set_tag_for_filter "psu-classes" "to:L-LAUS@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "psu-classes" "from:newswire@psu.edu" "${TAGS}"
set_tag_for_filter "psu-classes" "to:L-OFFCAMPUS-ANNOUNCEMENTS@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "psu-classes" "to:L-UHS-ALERTS@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "psu-classes" "from:engr.psu.edu" "${TAGS}"
set_tag_for_filter "psu-classes" "to:L-CLJ-MAJORS@LISTS.PSU.EDU" "${TAGS}"
set_tag_for_filter "psu-classes" "from:sa.psu.edu" "${TAGS}"

# dream
set_tag_for_filter "byronsanchez-dream" "from:info@dreamviews.org" "${TAGS}"
set_tag_for_filter "byronsanchez-dream" "from:forum@ld4all.com" "${TAGS}"
set_tag_for_filter "byronsanchez-dream" "from:info@dreamviews.com" "${TAGS}"

# sales
set_tag_for_filter "byronsanchez-sales" "from:salespractice.com" "${TAGS}"
set_tag_for_filter "byronsanchez-sales" "from:carjobsonline.com" "${TAGS}"

# social
set_tag_for_filter "byronsanchez-social" "from:voice-noreply@google.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:facebookmail.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:plus.google.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:noreply@couchsurfing.org" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:noreply@youtube.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:twitter.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:invitations@linkedin.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:security-noreply@linkedin.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:notifications@redditmail.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:gab.ai" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:txt.voice.google.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "to:txt.voice.google.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:snapchat.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:skype.com" "${TAGS}"
set_tag_for_filter "byronsanchez-social" "from:yelp.com" "${TAGS}"

# dating
set_tag_for_filter "byronsanchez-dating" "from:franandersoncyc@gmail.com" "${TAGS}"
set_tag_for_filter "byronsanchez-dating" "from:fran@conqueryourcampus.com" "${TAGS}"
set_tag_for_filter "byronsanchez-dating" "from:bounces@mail1.oknotify2.com" "${TAGS}"
set_tag_for_filter "byronsanchez-dating" "from:customercare@plentyoffish.com" "${TAGS}"
set_tag_for_filter "byronsanchez-dating" "from:customercare@pof.com" "${TAGS}"
set_tag_for_filter "byronsanchez-dating" "from:pof.com" "${TAGS}"


# the-project
# this goes last since last rule "wins" against the google social media rules
#
# NOTE: SINGLE QUOTES DON'T WORK AS EXPECTED!!!
set_tag_for_filter "the-project" "subject:\"[The Project]\"" "${TAGS}"

# Ensure maildir matches these newly set tags
${HOME}/.dotfiles/bin/make-maildir-match-tags.sh


