
# NOTE: This is a script that implements the subset of gentoo-provision that
# creates a USB keyflie. It is /incomplete/. The reason I am keeping it here is
# in case I move forward with fedora+dracut to create a keyfile, this might be
# useful.

# Creates a LUKS keyfile encrypted with GPG for 2FA on a USB flash drive

## 1. Configure USB for booting
##    1. Format and mount drive
##    2. Create pseudo-random blob and encrypt it
## 2. Add keyfile to LUKS volume

printf "Retrieving USB block device... ";
printf "Hello, ${USER}.  We will now create a bootable USB key for your new workstation...\n\n"
sleep 2
printf "Please make sure the usb device is currently connected and unmounted...\n\n"
printf "Please note the block device for the USB device and enter it (without the /dev/ portion) when prompted...\n\n"
lsblk
printf "\n"
printf "Please enter the USB block device (sdZ): "
read USB_DEVICE;
printf "done\n";

printf "Building EFI partition on USB block device /dev/${USB_DEVICE}... ";
sgdisk -Z /dev/${USB_DEVICE}
# Format drive
# o - clear existing gpt
sgdisk -a 2048 -o /dev/${USB_DEVICE}
# 2048 for optimal alignment
# 0 for end sector means all available space
sgdisk -n 1:2048:0 -c 1:"EFI System Partition" -t 1:ef00 /dev/${USB_DEVICE}
mkfs.vfat -F32 /dev/${USB_DEVICE}1
printf "done\n";

printf "Generating LUKS encryption key on EFI system partition /dev/${USB_DEVICE}... ";
# mount drive
mkdir -v /tmp/efiboot
mount -v -t vfat /dev/${USB_DEVICE}1 /tmp/efiboot
# Create pseudo-random blob and encrypt it
export GPG_TTY=$(tty) # needed to ensure pinentry works in screen
dd if=/dev/urandom bs=8388607 count=1 | gpg --symmetric --cipher-algo AES256 --output /tmp/efiboot/luks-key.gpg
printf "done\n";

printf "Retrieving drive block device... ";
printf "We will now add the key file to your LUKS partition...\n\n"
sleep 2
printf "Please note the block device for the encrypted drive and enter it (without the /dev/ portion) when prompted...\n\n"
lsblk
printf "\n"
printf "Please enter the drive block device (sdZn): "
read DRIVE_DEVICE;
printf "done\n";

printf "Adding keyfile to LUKS volume on /dev/${DRIVE_DEVICE}... ";
# Add keyfile to LUKS partition
gpg --decrypt /tmp/efiboot/luks-key.gpg | cryptsetup --key-file - luksAddKey /dev/${DRIVE_DEVICE}
sleep 4
# print out info just because
cryptsetup luksDump /dev/${DRIVE_DEVICE}
# backup the header
cryptsetup luksHeaderBackup /dev/${DRIVE_DEVICE} --header-backup-file /tmp/efiboot/luks-header.img
printf "done\n";

sleep 4

umount /tmp/efiboot

