#!/bin/sh

# converts evernote png files into a single pdf doc. Sorting is necessary to 
# ensure the pages in the pdf are in date order.

#ls Evernote*.png | sort -n | tr '\n' ' ' | sed 's/$/\ mydoc.pdf/' | xargs 
#convert

# list the evernote files in the current directory
#
# sort in numerical order instead of by letter (numerical means that the 
# standard filesystem sorting from evernote.png evernote (1).png will be 
# respected instead of placing that first file without a number last.
#
# replace spaces in the filenames with an escaped space. This is needed since 
# xargs will iterate over spaces later. Without this, file names with spaces 
# would treat each 'piece' as a distinct file and would have the command fail 
# since those files obviously don't exist.
#
# strip newlinews and replace them with spaces
#
# append the pdf filename to the end of the string we've been building so far
#
# pass the string to imagemagick's convert command to convert the png files to 
# pdf format
ls Evernote*.png | sort -n | sed 's| |\\ |g' | tr '\n' ' ' | sed 's/$/\ mydoc.pdf/' | xargs convert

