#!/bin/sh

DISPLAY=":0.0"
export DISPLAY
wallpapers="${HOME}/Pictures/wallpapers";
feh --recursive --randomize --bg-fill $wallpapers
