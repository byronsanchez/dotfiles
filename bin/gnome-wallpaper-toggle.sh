#!/bin/sh

wallpapers="${HOME}/Pictures/wallpapers";

selection=$(find $wallpapers -type f -name "*.jpg" -o -name "*.png" | shuf -n1)
gsettings set org.gnome.desktop.background picture-uri "file://$selection"
# '<zoom|centered|none|scaled|spanned|stretched|wallpaper|zoom>'
gsettings set org.gnome.desktop.background picture-options 'zoom'

