#!/bin/bash
#
# source: https://bbs.archlinux.org/viewtopic.php?id=171144
# source: https://bbs.nextthing.co/t/automating-bluetoothctl/8869/2

#controller=""
device="1C:52:16:69:CA:D7"

# run the program bluez
bluetoothctl << EOF
power on
connect ${device}
EOF

# bluetooth ctl is command driven, thus it can be automated using here-docs
# source: http://tldp.org/LDP/abs/html/here-docs.html
#
# you can also use \t to tab the mac address instead of specifying it as a 
# variable
#bluetoothctl << EOF
#select ${controller}
#power on
#agent on
#default-agent
#discoverable on
#pairable on
#scan on
#pair ${device}
#trust ${device}
#connect ${device}
#quit
#EOF



