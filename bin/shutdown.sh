#!/bin/sh

shred -zuv ~/AppData/Local/Temp/*tramp*
find ~/tmp -type f -exec shred -zuv {} \;
rm -rf ~/tmp
find /tmp -type f -exec shred -zuv {} \;
rm -rf /tmp

shutdown /r /t 0

