#!/bin/sh
#
# A script to restore permissions of checked out files to that of the last 
# commit.
#
# The reason I have this is because during my fossil-to-git migration, a lot of 
# my checked out files had permissions of 0755 while get checks them out to 
# 0644. The 644 seems correct since it's less permissive and the default of my 
# linux machines. I wonder if Windows had something to do with the 755s.
#
# Anyways, this scripts just reverts only the permissions of checked out files 
# to their last saved state so I don't have to manually manage that mess.

# The 2:6 substring is needed because by default it gives perms of the form 
# 100644, and it looks like chmod isn't liking that.
git ls-tree -r HEAD|while read mode _ _ fpath; do chmod "${mode:2:6}" "$fpath"; done
