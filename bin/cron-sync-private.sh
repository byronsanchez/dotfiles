#!/bin/sh
# sync Private directory from windows to mira so it can get backed up

if [ ! -f "/var/lib/nitelite/backup/private-hourly" ];
then
  printf "Generating log file...\n";
  sudo touch /var/lib/nitelite/backup/private-hourly;
  sudo chown byronsanchez:byronsanchez /var/lib/nitelite/backup/private-hourly;
fi

printf "\nSync started: [$(date +"%Y-%m-%d %a %H:%M:%S %Z")]\n\n" >> /var/lib/nitelite/backup/private-hourly;

rsync -zptlrv --progress --delete --rsh='ssh' "navi:/cygdrive/c/Users/bfs50/Private/" "/home/byronsanchez/Private/" >> /var/lib/nitelite/backup/private-hourly; 2>&1 

if [ $? -eq 0 ];
then
  printf "\nSync completed: [$(date +"%Y-%m-%d %a %H:%M:%S %Z")]\n\n" >> /var/lib/nitelite/backup/private-hourly;
else
  printf "\nSync with errors: [$(date +"%Y-%m-%d %a %H:%M:%S %Z")]\n\n" >> /var/lib/nitelite/backup/private-hourly;
fi

