
extension="${1##*.}"

ffmpeg -i "${1}" -c:v copy \
    -ac 2 -af "pan=stereo|FL=FC+0.30*FL+0.30*BL|FR=FC+0.30*FR+0.30*BR" \
    "${1%.*}.stereo.${extension}"



