#!/bin/sh
# Add org file changes to the repository
REPOS="${HOME}/nitelite/notebook"

for REPO in $REPOS
do
    # old fossil version

    # echo "Repository: $REPO"
    # cd $REPO
    # Add new files
    # Remove deleted files
    # fossil addremove >/dev/null 2>&1
    # Commit with current datetime as message and ignore warnings like for CR/LF 
    # endings
    # fossil commit --no-warnings -m "$(date)"

    # Don't do anything if the repository does not exist, aka the notebook encfs 
    # is not mounted
    if [ -e "$REPO/.git" ];
    then
        echo "Repository: $REPO"
        cd $REPO
        # Remove deleted files
        git ls-files --deleted -z | xargs -0 git rm >/dev/null 2>&1
        # Add new files
        git add . >/dev/null 2>&1
        git commit -m "$(date +"%Y-%m-%d %a %H:%M:%S %Z")"
    else
      echo "Repo ${REPO} not mounted. Skipping..."
    fi
done

