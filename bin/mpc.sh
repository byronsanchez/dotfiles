#!/bin/bash
#
# Handles media commands and outputs notifications

source $HOME/.dotfiles/scripts/helpers/common.lib.sh

###############
# CONFIGURATION

# Set defaults
server="127.0.0.1"
port=6600
length=10
title="MPD Control"
message=""
notifier=""
action=""

############################
# Argument Parsing Functions

function show_help ()
{
cat <<-EOM

$0 [OPTION] command

options:

    -s --server=NAME    host ip for the mpd server
    -p --port=NAME      host port for the mpd server
    -l --length=NAME    number of songs to add for append commands; default is 10
    -t --title=NAME     title for notifications; default is 'MPD Control'
    -m --message=NAME   notification message; default is command result
    -n --notifier=NAME  notification command to execute; if none, no notification will be sent

commands:

    prev                  play the previous song in the playlist
    next                  play the next song in the playlist
    toggle                toggle between play and pause
    random                clear the playlist and add n random songs from the library
    random-append         append n random songs from the library to the current playlist
    album                 clear the playlist and add a random album from the library
    album-append          append a random album from the library to the current playlist
    album-current         clear the playlist and play the album of the current song
    album-current-append  append the album of the current song to the current playlist
    volume-decrease       decreases mpd volume
    volume-increase       increases mpd volume
    shuffle-toggle        toggle shuffle between on and off
    single-toggle         toggle repeat-song between on and off
    repeat-toggle         toggle loop-platlist between on and off
    update                run an mpd library update
    current               print the currently playing song

EOM
    exit 1
}

function get_options () {
    argv=()
    while [ $# -gt 0 ]
    do
        opt=$1
        shift
        case ${opt} in
            -s|--server)
                server=$1
                shift
                ;;
            --server=*)
                server=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -p|--port)
                port=$1
                shift
                ;;
            --port=*)
                port=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -l|--length)
                length=$1
                shift
                ;;
            --length=*)
                length=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -t|--title)
                title=$1
                shift
                ;;
            --title=*)
                title=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -m|--message)
                message=$1
                shift
                ;;
            --message=*)
                message=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -n|--notifier)
                notifier=$1
                shift
                ;;
            --notifier=*)
                notifier=$(echo ${opt} | cut -d'=' -f 2);
                ;;
            -h|--help)
                show_help
                ;;
            *)
                if [ "${opt:0:1}" = "-" ]; then
                    fail "${opt}: unknown option."
                else
                  # The action is the first argument passed with no associated 
                  # option. If more than one is passed, the most recent will 
                  # override and will be the action that is executed.
                  action=${opt}
                fi
                argv+=(${opt})
                ;;
        esac
    done
}

############
# VALIDATION

# Parse options if they were passed
get_options $*

if [ ! -n "$action" ];
then
  fail "Please provide a valid command to execute"
fi

#############
# MAIN SCRIPT

unset message

case "$action" in
prev)
  mpc --no-status --host $server --port $port prev
  message="$(mpc --host $server --port $port status | sed '1 ! d')"
  ;;
next)
  mpc --no-status --host $server --port $port next
  message="$(mpc --host $server --port $port status | sed '1 ! d')"
  ;;
toggle)
  mpc --no-status --host $server --port $port toggle
  if mpc --host $server --port $port status | grep '\[playing\]' >/dev/null; 
  then
    # show the artist and the song name
    message="playing $(mpc --host $server --port $port status | sed '1 !  d')"
  else
    message='paused'
  fi
  ;;
random|random-append)
  if [ "$action" = "random-append" ];
  then
    newlength=$(($(mpc --host $server --port $port playlist | grep -c ^) + $length))
  else
    mpc --host $server --port $port clear
  fi

  while (( `mpc --host $server --port $port playlist | grep -c ^` < $length ));
  do
    song=`mpc --host $server --port $port listall | sed -n $[RANDOM % $(mpc --host $server --port $port stats | grep Songs | awk '{print $2}')+1]p`
    if [ "$song" ];
    then
     mpc -q --host $server --port $port add "$song"
    fi
  done

  message="${length} random songs added to playlist"
  ;;
album|album-append)
  if [ "$action" = "album" ];
  then
    mpc --host $server --port $port clear
  fi

  album=$(mpc --host $server --port $port list album | gshuf -n1);
  mpc --host $server --port $port search album "$album" | mpc --host $server --port $port add
  message="album '$album' added to playlist"
  ;;

album-current|album-current-append)
  album_current="$(mpc --host $server --port $port --format %album% current)"

  if [ "$action" == "album-current" ];
  then
    mpc --host $server --port $port clear
  fi

  mpc --host $server --port $port search album "$album_current" | mpc --host $server --port $port add
  message="Album '$album_current' added to playlist"
  ;;

volume-decrease)
  volume_state=$(mpc --host $server --port $port volume -1 | perl -nle'print $& if m{volume:.[a-z0-9%]+}');
  message="$volume_state"
  ;;
volume-increase)
  volume_state=$(mpc --host $server --port $port volume +1 | perl -nle'print $& if m{volume:.[a-z0-9%]+}');
  message="$volume_state"
  ;;
update)
  mpc --no-status --host $server --port $port update
  message='updating mpd library'
  ;;
shuffle)
  # Perl must be used since in Mountain Lion, grep's P switch is not
  # available
  shuffle_state=$(mpc --host $server --port $port random | perl -nle'print $& if m{random: .[a-z]+}');
  message="$shuffle_state"
  ;;
single)
  single_state=$(mpc --host $server --port $port single| perl -nle'print $& if m{single: .[a-z]+}');
  message="$single_state"
  ;;
repeat)
  repeat_state=$(mpc --host $server --port $port repeat | perl -nle'print $& if m{repeat: .[a-z]+}');
  message="$repeat_state"
  ;;

current)
  message=$(mpc --host $server --port $port current)
  ;;
*)
  fail "${action}: unknown command."
  ;;
esac

# Send the notification to the appropriate program
if [ -n "$notifier" ] && [ -n "$message" ];
then
  if [ $notifier = "terminal-notifier" ];
  then
    terminal-notifier -message "$message" -title "$title"
  elif [ $notifier = "ratpoison" ];
  then
    ratpoison -d :0 -c "echo [$title] $message"
  elif [ $notifier = "dunstify" ];
  then
    dunstify -r 1 "[$title]" "$message"
  else
    # treat the notifer as a command and pass it the title and message as a 
    # string
    $notifier "[$title] $message"
  fi
fi

