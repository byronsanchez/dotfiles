#!/bin/sh

shred -zuv ~/AppData/Local/Temp/*tramp*
rm -rf ~/AppData/Local/Temp/*
find ~/tmp -type f -exec shred -zuv {} \;
rm -rf ~/tmp
find /tmp -type f -exec shred -zuv {} \;
rm -rf /tmp
shred -zuv ~/.gnupg/*
shred -zuv ~/.ssh/*
rm -rf ${HOME}/projects/byronsanchez
${HOME}/.dotfiles/scripts/rm-links
# Removes this script!
rm -rf ~/.dotfiles/
rm -rf ~/.w3m/
rm -rf ~/.*
rm -f ~/dotfiles.fossil
rm -f ~/_viminfo
rm -rf ~/vimperator
rm -rf ~/vimfiles
rm -rf ~/Downloads/*
rm -rf ~/go/
rm -rf ~/News
find ~/.secrets -type f -exec shred -zuv {} \;
rm -rf ~/.secrets;
find ~/org -type f -exec shred -zuv {} \;
rm -rf ~/org
# delete the script itself
rm -- "$0"
# just in case the script couldn't delete itself the first time, this will try 
# to get rid of the parent directories of this script
rm -rf ~/.dotfiles/
shred -zuv ${HOME}/*history*
# ensure that zhistory gets shredded last or it may log some of the other shreds
shred -zuv ${HOME}/.zhistory

