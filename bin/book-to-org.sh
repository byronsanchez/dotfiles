#!/bin/sh
#
# source: http://thread.gmane.org/gmane.emacs.orgmode/75159

# 72 pt == 1 inch margins
ebook-convert "${1}" "${1%.*}.htmlz"
unzip -q "${1%.*}.htmlz"
pandoc index.html -o "${1%.*}.org"

# clean up intermediary files

# keep the images since pandoc will actually link to them and org-mode can 
# display them inline!
#rm -rf images/
rm -f *.html
rm -f "${1%.*}.htmlz"
rm -f *.opf
rm -f *.css

