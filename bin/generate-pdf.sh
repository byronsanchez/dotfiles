#!/bin/sh

# 72 pt == 1 inch margins
ebook-convert "${1}"  "${1%.*}.pdf" \
  --pdf-mono-family 'Ubuntu Mono for Powerline' \
  --pdf-serif-family 'ChareInk' --margin-bottom 72 --margin-left 72 \
  --margin-right 72 --margin-top 72 \
  --output-profile tablet

