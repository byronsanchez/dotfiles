#!/bin/bash
#
# Dependencies: AudioSwitcher, terminal-notifier

source $HOME/.dotfiles/scripts/helpers/common.lib.sh

###############
# CONFIGURATION

# Set defaults
notifier=""
config_file=""
asoundrc_jack="${HOME}/.dotfiles/alsa/.asoundrc.jack"
asoundrc_alsa="${HOME}/.dotfiles/alsa/.asoundrc.alsa"

quiet=0

############################
# Argument Parsing Functions

function show_help ()
{
cat <<-EOM

$0 [OPTION] command

options:

    -n --notifier=NAME  notification command to execute; if none, no notification will be sent
    -c --config-file=NAME       config file used to set the above values

commands:

    toggle                 toggle between audio devices
    toggle-sound-card      toggle between alsa and jack
    current-sound-card     print the current sound card
    volume-increase        increase system volume
    volume-decrease        decrease system volume
    toggle-mute            mute toggle

other:

    -q --quiet               do not log messages to STDOUT
    -h --help                display this message

EOM
    exit 1
}

function get_options () {
    argv=()
    while [ $# -gt 0 ]
    do
        opt=$1
        shift
        case ${opt} in
            -n|--notifier)
                notifier=$1
                shift
                ;;
            --notifier=*)
                notifier=$(echo ${opt} | cut -d'=' -f 2);
                ;;

            -q|--quiet)
                quiet=1
                ;;
            -h|--help)
                show_help
                ;;
            *)
                if [ "${opt:0:1}" = "-" ]; then
                    fail "${opt}: unknown option."
                else
                  # The action is the first argument passed with no associated
                  # option. If more than one is passed, the most recent will
                  # override and will be the action that is executed.
                  action=${opt}
                fi
                argv+=(${opt})
                ;;
        esac
    done
}

function notifier() {
  f_notifier=$1
  f_title=$2
  f_message=$3

  # Send the notification to the appropriate program
  if [ -n "$f_notifier" ] && [ -n "$f_message" ];
  then
    if [ $f_notifier = "terminal-notifier" ];
    then
      terminal-notifier -message "$f_message" -title "$f_title"
    elif [ $f_notifier = "ratpoison" ];
    then
      ratpoison -d :0 -c "echo [$f_title] $f_message"
    elif [ $notifier = "dunstify" ];
    then
      dunstify -r 1 "[$f_title]" "$f_message"
    else
      # treat the notifer as a command and pass it the title and message as a
      # string
      $f_notifier "[$f_title] $f_message"
    fi
  fi
}

############
# VALIDATION

# Parse options if they were passed
get_options $*

if [ ! -n "$action" ];
then
  fail "Please provide a valid command to execute"
fi

if [ ! -n "${config_file}" ];
then
  config_file="${HOME}/.dotfiles-settings/config"
fi

if [ -e "${config_file}" ];
then
  source "${config_file}"
fi



#############
# MAIN SCRIPT

unset message

case "$action" in
toggle)

  # TODO: implement toggle functionality for osx
  if [ "$(uname -s)" == "Darwin" ];
  then

    # terminal-notifier notification title
    title="core-audio"

    case "$cmd" in
    h4)
      AudioSwitcher -s "H4"
      message="core-audio output set to h4"
      ;;
    built-in)
      message="core-audio output set to built-in"
      AudioSwitcher -s "Built-in Output"
      ;;
    *)
      usage
    esac

  elif [ "$(uname -s)" == "Linux" ];
  then

    # terminal-notifier notification title
    title="jackd"

    notifier "${notifier}" "${title}" "toggling audio device..."

    aplay -l |grep -i h4 --silent
    isH4Connected=$?

    if [ $isH4Connected -eq 0 ];
    then

      ps aux | grep jackd | grep -v grep | grep -i h4 --silent
      isH4ServerRunning=$?

      if [ $isH4ServerRunning -eq 0 ];
      then
        killall jackd
        /usr/bin/jackd -t2000 -dalsa -dhw:Intel -r44100 -p1664 -n2 &
        message="jackd output set to built-in"
      else
        killall jackd
        /usr/bin/jackd -t2000 -dalsa -dhw:H4 -r44100 -p1664 -n2 &
        message="jackd output set to h4"
      fi

    else

      killall jackd
      /usr/bin/jackd -t2000 -dalsa -dhw:Intel -r44100 -p1664 -n2 &
      message="jackd output set to built-in"

    fi

    sleep 7

    /usr/local/bin/loop2jack

  fi
  ;;

toggle-sound-card)

  title="audio"

  if [ -n "${sound_card}" ];
  then
    if [ "${sound_card}" == "jack" ];
    then
      new_sound_card="alsa"
    else
      notifier "${notifier}" "${title}" "toggling audio device..."
      new_sound_card="jack"
    fi
  else
    # default to jack
    new_sound_card="jack"
  fi

  echo "changing sound card from $sound_card to $new_sound_card "
  echo $config_file

  key="sound_card"
  value="${new_sound_card}"
  sed -i -r "s/($key *= *\").*/\1$value\"/" ${config_file}
  message="sound card: ${new_sound_card}"

  # update asoundrc based on toggle
  if [ "${new_sound_card}" == "jack" ];
  then
    rm ${HOME}/.asoundrc
    ln -s ${asoundrc_jack} ${HOME}/.asoundrc
    sudo /etc/init.d/alsasound restart
    media-controller.sh toggle
  else
    killall jackd
    rm ${HOME}/.asoundrc
    ln -s ${asoundrc_alsa} ${HOME}/.asoundrc
    sudo /etc/init.d/alsasound restart
  fi

  ;;

current-sound-card)

  title="audio"
  message="current sound card: ${sound_card}"
  echo $message

  ;;

volume-increase)
  amixer set Master 1%+ unmute; dunstify -r 1 "[alsa]" "vol. --  $(amixer sget Master | tail -1 | tr -d '[]' | awk '{print $5}')"
  ;;

volume-decrease)
  amixer set Master 1%- unmute; dunstify -r 1 "[alsa]" "vol. --  $(amixer sget Master | tail -1 | tr -d '[]' | awk '{print $5}')"
  ;;

toggle-mute)
  amixer set Master toggle; dunstify -r 1 "[alsa]" "vol. --  $(amixer sget Master | tail -1 | tr -d '[]' | awk '{print $6}')"
  ;;

*)
  fail "${action}: unknown command."
  ;;

esac

notifier "${notifier}" "${title}" "${message}"

