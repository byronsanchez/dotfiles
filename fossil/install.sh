#fossil setting diff-command fossil_diff_wrapper
#fossil setting gmerge-command fossil_merge_wrapper

  # gvim -f -d -c "wincmd J" "$4" "$1" "$2" "$3"

# This is the cygwin ca-certs location. We have to specify using windows path 
# since this is the windows-native version of fossil
if [[ "$OSTYPE" == cygwin* ]]; then
  fossil setting ssl-ca-location "C:\tools\cygwin\etc\pki\ca-trust\extracted\pem\tls-ca-bundle.pem"
fi

