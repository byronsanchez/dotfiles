#! /usr/bin/env python2
from subprocess import check_output
import os

def get_pass(email_address):
    secret_dir = os.environ.get('SECRETDIR') + "/files"
    if secret_dir:
        return check_output("gpg --use-agent --quiet --batch -d " + secret_dir + "/"  + email_address + ".gpg", shell=True).strip("\n")
    else:
        return ""

 # Propagate gnus-expire flag
# TODO: Figure out if this is useful for gnus
import offlineimap.imaputil as IU
if not hasattr(IU, 'monkeypatchdone'):
    IU.flagmap += [('gnus-expire','E'),
                   ('gnus-dormant', 'Q'),
                   ('gnus-save', 'V'),
                   ('gnus-forward', 'W')]
    IU.monkeypatchdone = True

