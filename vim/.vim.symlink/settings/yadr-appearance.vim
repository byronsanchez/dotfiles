" Make it beautiful - colors and fonts

set background=dark
set ttyfast

if has("gui_running")
  "tell the term has 256 colors
  set t_Co=256
  if !has("win32")
    " linux gui gets normal wombat256mod
    colorscheme wombat256mod
  else
    " windows gui gets molokai 
    colorscheme molokai
  endif

  " Show tab number (useful for Cmd-1, Cmd-2.. mapping)
  " For some reason this doesn't work as a regular set command,
  " (the numbers don't show up) so I made it a VimEnter event
  autocmd VimEnter * set guitablabel=%N:\ %t\ %M

  set lines=60
  set columns=190
  
  if !has("win32")
    " errors out on windows
    if has("gui_gtk2")
      set guifont=Ubuntu\ Mono\ for\ Powerline11
    else
      set guifont=Ubuntu\ Mono\ for\ Powerline:h11
    end
  else
	  set guifont=Ubuntu_Mono_for_Powerlin:h11:cANSI
  end
else
  "dont load csapprox if we no gui support - silences an annoying warning
  let g:CSApprox_loaded = 1
  " 256 mode gets modded wombat.
  set t_Co=256
  colorscheme wombat256mod-nitelite-v2
endif

