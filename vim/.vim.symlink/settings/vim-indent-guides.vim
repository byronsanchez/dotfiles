let g:indent_guides_auto_colors = 0

" colors meant to match with my custom vim colors
"
" dark grey first
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=236
" light grey second
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=241

let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

