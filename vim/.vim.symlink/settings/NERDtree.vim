" Make nerdtree look nice
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let g:NERDTreeWinSize = 30

" Do not use arrows in the tree.
let g:NERDTreeDirArrows=0

" show hidden files in NERDTree
let NERDTreeShowHidden=1


