" If you want :UltiSnipsEdit to split your window.
" let g:UltiSnipsEditSplit = 'horizontal'
" 
" " Private snippets directory for editing
" let g:UltiSnipsSnippetsDir=$HOME . "/.dotfiles/vim/.vim.symlink/UltiSnips"
" let g:UltiSnipsSnippetDirectories = [ $HOME . "/.dotfiles/vim/.vim.symlink/UltiSnips", $HOME .  "/.dotfiles/vim/.vim.symlink/bundle/angular-styleguide/assets/vim-angular-ultisnips" ]
" 
" 
" " Snippets directory for Angular (papa's styleguide)
" "let g:UltiSnipsSnippetsDir=$HOME . 
" ""/.dotfiles/vim/.vim.symlink/bundle/angular-styleguide/assets/vim-angular-ultisnips"
" 
" " Disable tabs for ycm; will just use up and down arrow keys
" let g:ycm_key_list_select_completion=["<Down>"]
" let g:ycm_key_list_previous_completion=["<Up>"]
" " Prevent collision with vim-gnupg autoclose
" let g:ycm_allow_changing_updatetime = 0
" 
" " Add non-default language js development snippets
" autocmd InsertEnter *.js UltiSnipsAddFiletypes javascript-angular
" " papa styleguide
" autocmd InsertEnter *.js UltiSnipsAddFiletypes javascript_angular
" autocmd InsertEnter *.js UltiSnipsAddFiletypes javascript-jasmine
" autocmd InsertEnter *.js UltiSnipsAddFiletypes javascript-jsdoc
" autocmd InsertEnter *.js UltiSnipsAddFiletypes json
" autocmd InsertEnter *.js UltiSnipsAddFiletypes javascript.node
" 
" autocmd InsertEnter *.coffee UltiSnipsAddFiletypes coffee-jasmine
" autocmd InsertEnter *.coffee UltiSnipsAddFiletypes angular_coffee
" 
" " make YCM compatible with UltiSnips (using supertab)
" let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
" let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
" let g:SuperTabDefaultCompletionType = '<C-n>'
" 
" " better key bindings for UltiSnipsExpandTrigger
" let g:UltiSnipsExpandTrigger = "<tab>"
" let g:UltiSnipsJumpForwardTrigger = "<tab>"
" let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

