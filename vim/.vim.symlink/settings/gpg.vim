""""""""""""""""""""
" GnuPG Extensions "
""""""""""""""""""""

" Set the GnuPG Executable location
"
" We explicitly set this because on Fedora, the distro ships with gpg2 and gpg1.  
" On my other machines gpg == v2, but on fedora you have gpg == v1 + gpg-agent 
" == v2, which causes a communication match between gpg decrypts deferring to 
" the running v2 agent
" 
" So we set the gpg2 executable explicitly to handle this case. Cygwin should 
" just work with either executable since gpg == v2 for cygwin
" 
" If it fails (gpg2 bin not accessible in cygwin), there's a zsh alias setup for 
" cygwin only that alias gpg2 to gpg. I forgot why I put it there originally, 
" but can be used for resolving this specified bin.
"
" NOTE: 2018-05-09 - Vim wasn't reading the zsh gpg2 alias on windows, so I 
" reverted the executable back to gpg. If you switch to linux as daily driver, 
" you're gonna have to find a way to handle the gpg2 vs. gpg case between 
" operating systems because zsh aliases aren't working here.
"
" NOTE: Setting the executable to gpg makes saving encrypting files fail on
" Windows. Make sure it's commented out completely for it to work on Windows.
"
" let g:GPGExecutable="gpg2"

" Tell the GnuPG plugin to armor new files.
let g:GPGPreferArmor=1

" Tell the GnuPG plugin to sign new files.
let g:GPGPreferSign=1

augroup GnuPGExtra
  " Set extra file options.
  autocmd BufReadCmd,FileReadCmd *.\(gpg\|asc\|pgp\) call SetGPGOptions()
  " Automatically close unmodified files after inactivity.
  autocmd CursorHold *.\(gpg\|asc\|pgp\) quit
augroup END

function SetGPGOptions()

  " Set updatetime to 1 minute.
  set updatetime=60000
  " Fold at markers.
  set foldmethod=marker
  " Automatically close all folds.
  set foldclose=all
  " Only open folds with insert commands.
  set foldopen=insert

  " conceal passwords
  " press enter on a password to reveal it. alternatively, yank to + or * buffer 
  " to copy password to clipboard without revealing it on-screen
  set conceallevel=1
  set concealcursor=vinc
  syn match pass /pass: \zs.*/ conceal cchar=*
  function! s:ToggleDisplay()
    if &concealcursor == ""
      set concealcursor=vinc
    else
      set concealcursor=
    endif
  endfunction
  nmap <silent> <buffer> <CR> :call <SID>ToggleDisplay()<CR>

  " Yank WORD to system clipboard in normal mode
  nmap <leader>y "+yE
  " Yank selection to system clipboard in visual mode
  vmap <leader>y "+y

endfunction
