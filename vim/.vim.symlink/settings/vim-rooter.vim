" Stop vim-rooter from echoing the project directory.
let g:rooter_silent_chdir = 1

" Locally change directory.
let g:rooter_use_lcd = 1

let g:rooter_patterns = [
  \   '.git/', '.hg/', '.fossil-settings/',
  \   'Makefile', 'setup.py', 'Rakefile', 'build.xml', 'build.gradle',
  \   'requirements.txt', 'Gemfile', 'composer.json',
  \   'package.json', 'node_modules/',
  \   'README', 'README.txt', 'README.rst', 'README.md', 'README.mkd', 'README.markdown',
  \   'Guardfile',
  \   'Vagrantfile',
  \ ]

let g:rooter_change_directory_for_non_project_files = 0

" Only trigger rooter manually
let g:rooter_manual_only = 1
" custom trigger
map <silent> <unique> <Leader>r <Plug>RooterChangeToRootDirectory

