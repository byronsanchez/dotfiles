" Use 80 columns to display fold text.
let g:ledger_maxwidth = 80

" Use a · to connect the account name to the amount in fold text.
let g:ledger_fillstring = '·'

augroup LedgerExtra
  " Set extra file options
  autocmd FileType ledger call SetLedgerOptions()
  " Automatically close unmodified files after inactivity.
  "autocmd CursorHold *.\(ledger\) quit
augroup END

function SetLedgerOptions()
  " Hide standard fold fillchars (dashes)
  set fillchars=vert:\|
  " 1 minute updatetime
  "set updatetime=60000
endfunction

" Mark a transaction as cleared
nnoremap <leader>ct :call ledger#transaction_state_toggle(line('.'))<CR>

" Toggle between states
nnoremap <leader>to :call ledger#transaction_state_toggle(line('.'), ' *!?')<CR>

" Set a transaction's primary date to today
nnoremap <leader>dt :call ledger#transaction_date_set(line('.'), 'unshift')<CR>

