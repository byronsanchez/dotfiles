"
" Terminal setup.
"
set background=dark
if version > 580
    highlight clear
    if exists("g:syntax_on")
        syntax reset
    endif
endif
let g:colors_name="shblah"

"
" Highlighting definitions.
"

    "
    " Actual colours and styles.
    "
    highlight Normal       term=NONE cterm=NONE ctermfg=252    ctermbg=NONE
    highlight Cursor       term=NONE cterm=bold ctermfg=192    ctermbg=NONE
    " this gets in the way for vimdiffs
    "highlight CursorLine   term=NONE cterm=NONE ctermfg=NONE ctermbg=236
    highlight CursorLine   term=NONE cterm=NONE ctermfg=NONE ctermbg=235
    highlight ColorColumn  term=NONE cterm=NONE ctermfg=NONE ctermbg=236

    hi! link FoldColumn		Folded
    highlight Comment      term=NONE cterm=bold ctermfg=244    ctermbg=NONE
    highlight Function     term=NONE cterm=bold ctermfg=192    ctermbg=NONE
    highlight Identifier   term=NONE cterm=bold ctermfg=192    ctermbg=NONE
    highlight Constant     term=NONE cterm=bold ctermfg=221    ctermbg=NONE
    highlight Statement    term=NONE cterm=bold ctermfg=159    ctermbg=NONE
    highlight String       term=NONE cterm=NONE ctermfg=192    ctermbg=NONE
    highlight PreProc      term=NONE cterm=bold ctermfg=221    ctermbg=NONE
    highlight Type         term=NONE cterm=NONE ctermfg=229    ctermbg=NONE

    highlight Search       term=NONE cterm=bold ctermfg=177    ctermbg=241
    highlight Special      term=NONE cterm=bold ctermfg=229    ctermbg=NONE
    highlight SpecialKey   term=NONE cterm=NONE ctermfg=236    ctermbg=NONE
    highlight Pmenu        term=NONE cterm=NONE ctermfg=230    ctermbg=238
    highlight NonText      term=NONE cterm=bold ctermfg=244    ctermbg=NONE
    highlight Folded       term=NONE cterm=bold ctermfg=103    ctermbg=NONE

    " highlight StatusLineNC term=NONE cterm=bold ctermfg=246    ctermbg=NONE
    highlight Todo         term=NONE cterm=NONE ctermfg=101    ctermbg=NONE
    highlight VertSplit    term=NONE cterm=bold ctermfg=238    ctermbg=NONE
    highlight Visual       term=NONE cterm=bold ctermfg=234    ctermbg=249
    highlight StatusLine   term=NONE cterm=bold ctermfg=230    ctermbg=NONE

    " highlight DiffAdd      term=NONE cterm=NONE ctermfg=233 ctermbg=194
    " highlight DiffDelete   term=NONE cterm=NONE ctermfg=249 ctermbg=224
    " highlight DiffChange   term=NONE cterm=NONE ctermfg=233 ctermbg=255
    " highlight DiffText     term=NONE cterm=NONE ctermfg=233 ctermbg=177

    highlight DiffAdd      term=NONE cterm=NONE ctermbg=108
    highlight DiffDelete   term=NONE cterm=NONE ctermbg=161
    "highlight DiffChange   term=NONE cterm=NONE ctermbg=255
    highlight DiffChange   term=NONE cterm=NONE ctermbg=244
    highlight DiffText     term=NONE cterm=NONE ctermbg=177

    " For marks a-z
    hi clear ShowMarksHLl
    highlight ShowmarksHLl       term=NONE cterm=bold ctermfg=177    ctermbg=241
    " For marks A-Z
    hi clear ShowMarksHLu
    highlight ShowmarksHLu       term=NONE cterm=bold ctermfg=234    ctermbg=249

    "
    " General highlighting group links.
    "
    highlight! link diffAdded       DiffAdd
    highlight! link diffRemoved     DiffDelete
    highlight! link diffChanged     DiffChange
    highlight! link StatusLineNC    StatusLine
    highlight! link Title           Normal
    highlight! link LineNr          NonText
    highlight! link TabLine         StatusLineNC
    highlight! link TabLineFill     StatusLineNC
    highlight! link TabLineSel      StatusLine
    highlight! link VimHiGroup      VimGroup

    " Test the actual colorscheme
    syn match Comment      "__Comment.*"
    syn match Constant     "__Constant.*"
    syn match Cursor       "__Cursor.*"
    syn match CursorLine   "__CursorLine.*"
    syn match DiffAdd      "__DiffAdd.*"
    syn match DiffChange   "__DiffChange.*"
    syn match DiffDelete   "__DiffDelete.*"
    syn match Folded       "__Folded.*"
    syn match Function     "__Function.*"
    syn match Identifier   "__Identifier.*"
    syn match IncSearch    "__IncSearch.*"
    syn match NonText      "__NonText.*"
    syn match Normal       "__Normal.*"
    syn match Pmenu        "__Pmenu.*"
    syn match PreProc      "__PreProc.*"
    syn match Search       "__Search.*"
    syn match Special      "__Special.*"
    syn match SpecialKey   "__SpecialKey.*"
    syn match Statement    "__Statement.*"
    syn match StatusLine   "__StatusLine.*"
    syn match StatusLineNC "__StatusLineNC.*"
    syn match String       "__String.*"
    syn match Todo         "__Todo.*"
    syn match Type         "__Type.*"
    syn match VertSplit    "__VertSplit.*"
    syn match Visual       "__Visual.*"

