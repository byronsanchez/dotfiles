#!/usr/bin/env bash

set -e

vundle_dst="$HOME/.vim/bundle/Vundle.vim"

# Make sure vundle is installed
if [ ! -d "$vundle_dst"  ]
then
  git clone https://github.com/gmarik/Vundle.vim.git $vundle_dst
fi

# Install or update existing plugins
#vim +PluginInstall! +qall 2&> /dev/null
vim +PluginInstall +qall

